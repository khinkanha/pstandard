<?php
class ErrorController extends Controller {
	public function index() {
		$this->view->render('error/default');
	}
	public function code($error_name) {
		$this->vars->error_name = $error_name;
		$this->view->render('error/defaultcode', $this->vars);
	}
}