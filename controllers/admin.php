<?php

class Admin extends AdminController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {}

    public function members($page = 1)
    {
        $page = intval($page);
        $offset = 0;
        $limit = 15;
        $crit = "";
        if ($page > 0) {
            $offset = ($page - 1) * $limit;
        }
        $error_cnt = 0;
        $frm = new Form("frmsearch", "/admin/members/", "POST");
        $obj_search = FORM::GetObjInput("search", NULL, "TEXT", TRUE, 4);
        $error_cnt += $obj_search->validate();
        if (isPostBack()) {
            $crit = $obj_search->value;
        }
        
        $frm->version = 4;
        $frm->create_obj_input(1, $obj_search);
        $frm->submit("cmd", "Search", "Search", 2);
        $frm->create_inline_submit(2);
        $frm->create_row($obj_search->error);
        
        $_list = UserModel::_list($crit, $limit, $offset);
        $paging = new Paging(UserModel::get_total_record($crit), $limit, $page);
        $paging->set_link('/admin/members/');
        $this->vars->paging = $paging->generate_link();
        $this->vars->frmsearch = $frm->generate();
        $this->vars->members = $_list;
        $this->_view('/backend/admin/member');
    }

    public function member_dashboard($username)
    {
        $a_user = UserModel::getUsername($username);
        $this->vars->member = $a_user;
        $this->_view('/backend/admin/member_dashboard');
    }

    public function user_profile($username)
    {
        /**
         *
         * @var UserModel $a_user
         */
        $error_cnt = 0;
        $frm = new Form("frm_profile", "/admin/user_profile/" . $username, "POST");
        $a_user = UserModel::getUsername($username);
        $obj_fullname = FORM::GetObjInput("fullname", $a_user->fullname, "TEXT", TRUE, 5);
        $obj_email = FORM::GetObjInput("email", $a_user->email, "EMAIL", TRUE, 7);
        $obj_phone = FORM::GetObjInput("phone", $a_user->phone, "TEXT", NULL, 0);
        $obj_password = FORM::GetObjInput("password", NULL, "PASSWORD", TRUE, 5);
        $error_cnt += $obj_fullname->validate();
        $error_cnt += $obj_fullname->validate();
        $error_cnt += $obj_phone->validate();
        $error_cnt += $obj_password->validate();
        if (isPostBack()) {
            // check password
            if ($this->user->password != md5($obj_password->value)) {
                $error_cnt ++;
                $obj_password->error = "Password incorrect";
                $obj_password->is_error = true;
            }
            if (! validate_email($obj_email->value)) {
                $error_cnt ++;
                $obj_email->is_error = true;
                $obj_email->error = "email not valid";
            }
            
            if (! $error_cnt) {
                UserModel::update_info($obj_fullname->value, $obj_email->value, $obj_phone->value, $a_user->user_id);
                header('Location:/admin/member_dashboard/' . $username . '');
                exit();
            }
        }
        
        $frm->version = 4;
        $frm->create_h3("Update <span class='text-info'>" . $username . "</span> 's profile");
        $frm->create_obj_input(1, $obj_fullname, "Full Name");
        $frm->create_row($obj_fullname->error);
        $frm->create_obj_input(1, $obj_email, "Email");
        $frm->create_row($obj_email->error);
        $frm->create_obj_input(2, $obj_phone, "Phone");
        $frm->create_row($obj_phone->error);
        $frm->create_obj_input(1, $obj_password, "Your Password");
        $frm->create_row($obj_password->error);
        $this->vars->frm = $frm->generate();
        $this->_view('/backend/admin/frm_user');
    }

    public function user_bank($username)
    {
        /**
         *
         * @var UserModel $a_user
         */
        $error_cnt = 0;
        $frm = new Form("frm_bank", "/admin/user_bank/" . $username, "POST");
        $a_user = UserModel::getUsername($username);
        $obj_bank = FORM::GetObjInput("bank", $a_user->bank_name, "TEXT", TRUE, 3);
        $obj_account = FORM::GetObjInput("email", $a_user->account_name, "TEXT", TRUE, 7);
        $obj_accnumber = FORM::GetObjInput("phone", $a_user->account_no, "TEXT", NULL, 7);
        $obj_password = FORM::GetObjInput("password", NULL, "PASSWORD", TRUE, 5);
        $error_cnt += $obj_bank->validate();
        $error_cnt += $obj_accnumber->validate();
        $error_cnt += $obj_account->validate();
        $error_cnt += $obj_password->validate();
        if (isPostBack()) {
            // check password
            if ($this->user->password != md5($obj_password->value)) {
                $error_cnt ++;
                $obj_password->error = "Password incorrect";
                $obj_password->is_error = true;
            }
            
            if (! $error_cnt) {
                UserModel::update_bank($obj_bank->value, $obj_account->value, $obj_accnumber->value, $a_user->user_id);
                header('Location:/admin/member_dashboard/' . $username . '');
                exit();
            }
        }
        
        $frm->version = 4;
        $frm->create_h3("Update <span class='text-info'>" . $username . "</span> 's bank information");
        $frm->create_obj_input(1, $obj_bank, "Bank Name");
        $frm->create_row($obj_bank->error);
        $frm->create_obj_input(1, $obj_account, "Account Name");
        $frm->create_row($obj_account->error);
        $frm->create_obj_input(2, $obj_accnumber, "Account Number");
        $frm->create_row($obj_accnumber->error);
        $frm->create_obj_input(1, $obj_password, "Your Password");
        $frm->create_row($obj_password->error);
        $this->vars->frm = $frm->generate();
        $this->_view('/backend/admin/frm_user');
    }

    public function force_log($username)
    {
        $_SESSION['admin_id'] = $this->user->user_id;
        $obj_user = UserModel::getUsername($username);
        if ($obj_user) {
            $_SESSION['user_id'] = $obj_user->user_id;
            header('Location: /dashboard');
            exit();
        }
    }

    public function password($username)
    {
        /**
         *
         * @var UserModel $a_user
         */
        $error_cnt = 0;
        $frm = new Form("frm_bank", "/admin/password/" . $username, "POST");
        $a_user = UserModel::getUsername($username);
        $obj_password1 = FORM::GetObjInput("upassword", NULL, "PASSWORD", TRUE, 6);
        $obj_password = FORM::GetObjInput("password", NULL, "PASSWORD", TRUE, 5);
        
        $error_cnt += $obj_password1->validate();
        $error_cnt += $obj_password->validate();
        if (isPostBack()) {
            // check password
            if ($this->user->password != md5($obj_password->value)) {
                $error_cnt ++;
                $obj_password->error = "Password incorrect";
                $obj_password->is_error = true;
            }
            
            if (! $error_cnt) {
                UserModel::updatePassword($a_user->user_id, $obj_password1->value);
                header('Location:/admin/member_dashboard/' . $username . '');
                exit();
            }
        }
        
        $frm->version = 4;
        $frm->create_h3("Update <span class='text-info'>" . $username . "</span> 's password");
        $frm->create_obj_input(2, $obj_password1, "New Password");
        $frm->create_row($obj_password1->error);
        $frm->create_obj_input(1, $obj_password, "Your Password");
        $frm->create_row($obj_password->error);
        $this->vars->frm = $frm->generate();
        $this->_view('/backend/admin/frm_user');
    }

    public function withdraw_list($batch_id = 0)
    {
        $batch_id = intval($batch_id);
        $obj_batch = new WithdrawBatchModel();
        $obj_batch->pull_by_id($batch_id);
        $this->vars->batch = $obj_batch;
        $this->vars->col = WithdrawModel::get_list_by_batchid($batch_id);
        $this->_view('/backend/admin/withdrawlist');
    }

    public function withdraw($status = "", $page = 1)
    {
        $limit = 15;
        $offset = 0;
        $page = intval($page);
        if ($page > 1) {
            $offset = ($page - 1) * $limit;
        }
        $tb = new Table();
        $tb->add_header("Request Date");
        $tb->add_header("Amount");
        $tb->add_header("Status");
        
        // get record
        $records = WithdrawBatchModel::_list($status, $limit, $offset);
        foreach ($records as $item) {
            $tb->add_col($item->created_at);
            $tb->add_col(number_format($item->total_amount_paid, 4) . " $" . ' | <a href="/admin/withdraw_list/' . $item->batch_id . '">View</a>');
            $tb->add_col(ucwords($item->batch_status));
            $tb->add_row();
        }
        $paging = new Paging(WithdrawBatchModel::get_total_record($status), $limit, $page);
        $paging->set_link("/backend/admin/withdraw/" . $status . "/");
        
        $this->vars->paging = $paging->generate_link();
        $this->vars->tb = $tb->generate();
        $this->_view('/backend/admin/batch_list');
    }

    public function update_batch()
    {
        if (isPostBack()) {
            $status = get_post('batch_status');
            $batch_id = intval(get_post('batch_id'));
            $obj_batch = new WithdrawBatchModel();
            $obj_batch->pull_by_id($batch_id);
            $obj_batch->update_status($status);
            // alert telegram
            if ($status == WithdrawBatchModel::STATUS_COMPLETED) {
                if (! $is_debug) {
                    // TelegramQueue::AddQueue(TelegramSettings::ID_ADMIN_GROUP, "✅🏦🏦🏦💰 transfered to members amount " . number_format($obj_batch->total_amount_paid, 2) . ' $ for batch ' . $obj_batch->created_at);
                    // TelegramQueue::SEND(TelegramSettings::BOT_API);
                }
            }
            // redirect to admin/withdrawlist/$batch_id
            header('location: /admin/withdraw_list/' . $obj_batch->batch_id);
            exit();
        }
    }

    public function create_user()
    {
        $frm = new Form("add_user", "/admin/create_user", "POST");
        $error_count = 0;
        $obj_username = Form::GetObjInput('username', NULL, 'TEXT', TRUE, 8);
        $obj_fullname = Form::GetObjInput('fullname', NULL, 'TEXT', TRUE, 2);
        $obj_email = Form::GetObjInput('email', NULL, 'email', TRUE, 0, 150);
        $obj_password1 = Form::GetObjInput('password1', NULL, 'password', TRUE, 6);
        $obj_password2 = Form::GetObjInput('password2', NULL, 'password', TRUE, 6);
        $error_count += $obj_fullname->validate();
        $error_count += $obj_email->validate();
        $error_count += $obj_password1->validate();
        $error_count += $obj_password2->validate();
        $error_count += $obj_username->validate();
        if (isPostBack()) {
            if (! validate_email($obj_email->value)) {
                $error_count ++;
                $obj_email->is_error = true;
                $obj_email->error = "email not valid";
            }
            if (UserModel::getByEmail($obj_email->value)) {
                $error_count ++;
                $obj_email->is_error = true;
                $obj_email->error = "email already in used";
            }
            if (UserModel::getUsername($obj_username->value)) {
                $error_count ++;
                $obj_username->is_error = true;
                $obj_username->error = "username already in used";
            }
            if ($obj_password1->value != $obj_password2->value) {
                $error_count ++;
                $obj_password1->is_error = true;
                $obj_password1->error = "password not match";
            }
            if (! $error_count) {
                $a_user = new UserModel();
                $random = substr(str_shuffle("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 1) . substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@_!"), 0, 7);
                $a_user->register_user($obj_username->value, $obj_password1->value, $obj_email->value, $obj_fullname->value, "", $random);
                header('location:/admin/members');
                exit();
            }
        }
        $frm->create_h3("Create New User");
        $frm->create_obj_input(1, $obj_username, "ID Card");
        $frm->create_obj_input(2, $obj_fullname, "Full Name");
        $frm->create_row($obj_username->error);
        $frm->create_obj_input(1, $obj_email, "Email");
        $frm->create_row($obj_email->error);
        $frm->create_obj_input(1, $obj_password1, "Password");
        $frm->create_obj_input(2, $obj_password2, "Password Again");
        $frm->create_row($obj_password1->error);
        $this->vars->frm = $frm->generate();
        $this->_view('/backend/admin/frm_user');
    }
}