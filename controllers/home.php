<?php
require_once 'class/mailer.php';

class Home extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->check_login();
    }

    public function index()
    {
        $this->view->render("index");
    }

    public function check_login()
    {
        if (isset($_COOKIE['username'])) {
            $a_user = new UserModel();
            $a_user->getbyUsername($_COOKIE['username']);
            if ($a_user->user_id > 0) {
                if ($a_user->secure_code == $_COOKIE['secure_code']) {
                    if (! isset($_SESSION['user_id'])) {
                        $_SESSION['user_id'] = $a_user->user_id;
                    }
                    
                    header('location: /dashboard');
                    exit();
                }
            }
        }
    }

    public function logout()
    {
        unset($_SESSION['user_id']);
        unset($_SESSION['admin_id']);
        unset($_COOKIE['username']);
        unset($_COOKIE['secure_code']);
        setcookie('secure_code', '', time() - 3600);
        setcookie('username', '', time() - 3600);
        session_destroy(TRUE);
        header('location: /login/');
        exit();
    }

    /**
     * Handling Login Page
     */
    public function login()
    {
        $error_count = 0;
        $af = new Form('login', '/login', 'POST');
        $post_username = Form::GetObjInput('username', NULL, 'TEXT', TRUE, 5);
        $post_password = Form::GetObjInput('password', NULL, 'password', TRUE, '5');
        $error_count += $post_username->validate();
        $error_count += $post_password->validate();
        if (! $error_count && isPostBack()) {
            $a_user = new UserModel();
            $a_user->getbyUsername($post_username->value);
            if ($a_user->user_id > 0) {
                
                if ($a_user->password == md5($post_password->value)) {
                    // Set Motion for Login Success
                    $code = rnd_code(16);
                    $a_user->updateSecurecode($code);
                    setcookie('username', $a_user->username, time() + (86400 * 30), "/");
                    setcookie('secure_code', $code, time() + (86400 * 30), "/");
                    $_SESSION['user_id'] = $a_user->user_id;
                    header('Location: /dashboard/');
                    exit();
                }
            }
            $error_count ++;
        }
        
        $af->create_text('<p class="text-center"><a href="/"><img src="/asset/logo.png" style="max-width: 150px;" /></a></p>');
        $post_error = NULL;
        if ($error_count) {
            $post_error = "The user name or password is incorrect.";
            $af->create_text('<p class="text-center text-danger">' . $post_error . '</p>');
        }
        $af->create_obj_input(1, $post_username, NULL, 'Username');
        $af->create_row($post_username->is_error);
        $af->create_obj_input(1, $post_password, NULL, 'Password');
        $af->create_row($post_password->is_error);
        $af->submit('cmd', 'Login', '<span class="text-light">Log In</span>', 1, FALSE, TRUE);
        $this->vars->post_error = $post_error;
        $this->vars->login_form = $af->generate();
        $this->_view('login');
    }

    /**
     * Handling Sign Up Page
     */
    public function signup($sponosr = "")
    {
        
        /**
         * Collect postback validation error
         *
         * @var integer $error_count
         */
        $error_count = 0;
        $af = new Form('signup', '/signup', 'POST');
        $obj_username = Form::GetObjInput('username', NULL, 'TEXT', TRUE, 8);
        $obj_firstname = Form::GetObjInput('firstname', NULL, 'TEXT', TRUE, 2);
        $obj_lastname = Form::GetObjInput('lastname', NULL, 'TEXT', TRUE, 2);
        $obj_email = Form::GetObjInput('email', NULL, 'email', TRUE, 0, 150);
        $obj_password1 = Form::GetObjInput('password1', NULL, 'password', TRUE, 6);
        $obj_password2 = Form::GetObjInput('password2', NULL, 'password', TRUE, 6);
        $error_count += $obj_firstname->validate();
        $error_count += $obj_lastname->validate();
        $error_count += $obj_email->validate();
        $error_count += $obj_password1->validate();
        $error_count += $obj_password2->validate();
        $error_count += $obj_username->validate();
        
        if (isPostBack()) {
            if ($obj_password1->value != $obj_password2->value) {
                $obj_password1->is_error = TRUE;
                $obj_password1->error = "Password not match";
                $error_count ++;
            }
            if (! validate_username($obj_username->value, 5)) {
                $obj_username->is_error = TRUE;
                $obj_username->error = "Username is taken or invalid.";
                $error_count ++;
            }
            if (! $error_count) {
                /**
                 * Get current user object
                 *
                 * @var UserModel $a_user
                 */
                $a_user = UserModel::getUsername($obj_username->value);
                if ($a_user) {
                    $error_count ++;
                    $obj_username->is_error = TRUE;
                    $obj_username->error = "Username is taken";
                }
                if (! $error_count) {
                    // Insert registration
                    $random = substr(str_shuffle("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 1) . substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@_!"), 0, 7);
                    $auser = new UserModel();
                    $id = $auser->register_user($obj_username->value, $obj_password1->value, $obj_email->value, $obj_firstname->value . " " . $obj_lastname->value, 0, $random);
                    if ($id) {
                        // Send Confirmation E-mail
                        // send_welcome_email($a_user);
                        // Forward to waiting for confirmation page
                        // Telegram alert new user signup
                        header('Location: /login');
                        exit();
                    }
                }
            }
        }
        
        $af->create_text('<p class="text-center"><a href="/"><img src="/asset/logo.png" style="max-width: 150px;" /></a></p>');
        $af->create_h2("Register");
        $af->create_obj_input(1, $obj_username, NULL, 'ID Card');
        $af->create_row($obj_username->is_error);
        $af->create_obj_input(1, $obj_firstname, NULL, 'First Name');
        $af->create_obj_input(2, $obj_lastname, NULL, 'Last Name');
        $af->create_row($obj_firstname->is_error || $obj_lastname->is_error);
        $af->create_obj_input(1, $obj_email, NULL, 'Email');
        $af->create_row($obj_email->is_error);
        $af->create_obj_input(1, $obj_password1, NULL, 'Password');
        $af->create_row($obj_password1->is_error);
        $af->create_obj_input(1, $obj_password2, NULL, 'Confirm Password');
        $af->create_row($obj_password2->is_error);
        $af->create_checkbox('accept', 'accept', 'I accept the <a href="#" target="_blank">Terms of Use</a> &amp; <a href="#" target="_blank">Privacy Policy</a>', NULL, FALSE, FALSE, TRUE);
        $af->create_row();
        $af->submit('register', "Register", "Register Now", 2, FALSE, TRUE);
        $this->vars->signup_form = $af->generate();
        $this->_view('signup_form');
    }

    /**
     * Withdraw auto on Monday time 00:00 only happen for member and stock with balance 20
     */
    public function cron_withdraw()
    {
        $db = DB::getInstance();
        // insert batch id
        try {
            $db->beginTransaction();
            $stm = $db->select("SELECT user_id FROM user WHERE balance>20 AND account_name<>''");
            if ($stm != FALSE) {
                $wstm = $db->query("INSERT INTO withdraw_batch(batch_status,created_at) VALUES('created',NOW())");
                $batch_id = 0;
                if ($wstm != FALSE) {
                    $batch_id = $db->getLastInsertId();
                }
                $total_amount = 0;
                foreach ($stm as $user) {
                    $obj = new UserModel();
                    $obj->pull_by_id($user->user_id);
                    // start insert to withdraw list
                    $wt = new WithdrawModel();
                    $wt->created_by = $obj->user_id;
                    $wt->amount = $obj->balance;
                    $wt->bank_name = $obj->bank_name;
                    $wt->account_name = $obj->account_name;
                    $wt->account_number = $obj->account_no;
                    $wt->batch_id = $batch_id;
                    $wt->add();
                    $obj->update_wallet(- ($wt->amount));
                    $total_amount = $total_amount + $wt->amount;
                }
                $db->query("UPDATE withdraw_batch SET total_amount_paid=:total WHERE batch_id=:batch_id", array(
                    'total' => $total_amount,
                    'batch_id' => $batch_id
                ));
            }
            $db->commit();
        } catch (PDOException $e) {
            print_r($e);
            $db->rollback();
        }
    }

    public function reset_password($username = null, $code = NULL)
    {
        $this->vars->title = "Reset Password";
        $this->vars->post_email = NULL;
        $this->vars->post_email_err = 0;
        $error_msg = "";
        if (isset($_POST['cmdreset'])) {
            $post_email = get_post('email', 1);
            $this->vars->post_email = $post_email;
            // Check email exist or not
            $auser = UserModel::getByEmail($post_email);
            
            if (! $auser) {
                $error_msg .= "<li>Email Not found!</li>";
            } else {
                // @todo send email
                $random = substr(str_shuffle("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 1) . substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@_!"), 0, 7);
                UserModel::updateEmailCode($auser->user_id, $random);
                password_reset_mail($auser->email, $auser->fullname, $auser->username, $random);
                $this->vars->success_msg = "We already sent you the reset link!";
                unset($auser);
            }
        }
        if ($username != null and $code != null) {
            $this->vars->code = $code;
            $this->vars->post_username = $username;
            $user = new UserModel();
            $user->getbyUsername($username);
            if (! $user) {
                $error_msg .= "<li>Incorrect URL</li>";
            }
            if (isset($_POST['cmdchange'])) {
                
                if ($user->user_id > 0 and $user->verify_code == $code) {
                    // Verify Password
                    $new_pass = get_post('npassword', 1);
                    if (strlen($new_pass) < 6) {
                        $error_msg .= "<li>New password required at least 6 charactor!</li>";
                    } else {
                        // verify new password with the confirm new pass
                        $con_pass = get_post('conpassword', 1);
                        if ($new_pass != $con_pass) {
                            $error_msg .= "<li>The new password not match!</li>";
                        }
                    }
                    if (strlen($error_msg) > 0) {
                        $this->vars->error_msg = $error_msg;
                    } else {
                        UserModel::updatePassword($user->user_id, $new_pass);
                        UserModel::updateEmailCode($user->user_id, "");
                        $this->vars->success_msg = 'Password changed! <a href="/login">Login Now</a>';
                    }
                } else {
                    $this->vars->error_msg = "Incorrect URL";
                }
            }
            
            $this->view->render('reset_password_now', $this->vars);
        } else {
            $this->view->render('reset_password', $this->vars);
        }
    }

    public function verify($code)
    {
        if ($code != "") {
            $obj = new UserModel();
            $obj->getBycode($code);
            if ($obj->user_id > 0) {
                $obj->verify();
                $this->vars->success_msg = "Account Verify Success!";
            } else
                $this->vars->error_msg = "Verify code is invalid, Please contact to administrator";
        }
        $this->_view('index');
    }
}