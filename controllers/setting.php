<?php
class Setting extends UserController{
    public function __construct()
    
    {
        parent::__construct();
    }
    public function bank()
    {
        $error_cnt = 0;
        $frm = new Form("frm_bank", "/setting/bank/", "POST");
        $obj_bank = FORM::GetObjInput("bank", $this->user->bank_name, "TEXT", TRUE, 3);
        $obj_account = FORM::GetObjInput("email", $this->user->account_name, "TEXT", TRUE, 7);
        $obj_accnumber = FORM::GetObjInput("phone", $this->user->account_no, "TEXT", NULL, 7);
        $obj_password = FORM::GetObjInput("password", NULL, "PASSWORD", TRUE, 5);
        $error_cnt += $obj_bank->validate();
        $error_cnt += $obj_accnumber->validate();
        $error_cnt += $obj_account->validate();
        $error_cnt += $obj_password->validate();
        if (isPostBack()) {
            // check password
            if ($this->user->password != md5($obj_password->value)) {
                $error_cnt ++;
                $obj_password->error = "Password incorrect";
                $obj_password->is_error = true;
            }
            
            if (! $error_cnt) {
                UserModel::update_bank($obj_bank->value, $obj_account->value,$obj_accnumber->value, $this->user->user_id);
                header('Location:/setting/bank?success=true');
                exit();
            }
        }
       
        $frm->version = 4;
        $frm->create_h3("Bank Setting");
        $frm->create_obj_input(1, $obj_bank,"Bank Name");
        $frm->create_row($obj_bank->error);
        $frm->create_obj_input(1, $obj_account,"Account Name");
        $frm->create_row($obj_account->error);
        $frm->create_obj_input(2, $obj_accnumber,"Account Number");
        $frm->create_row($obj_accnumber->error);
        $frm->create_obj_input(1, $obj_password,"Your Password");
        $frm->create_row($obj_password->error);
        $this->vars->frm=$frm->generate();
        $this->_view('/backend/frm_user');
    }
    public function profile()
    {
        $error_cnt = 0;
        $frm = new Form("frm_profile", "/setting/profile/", "POST");
        $obj_fullname = FORM::GetObjInput("fullname", $this->user->fullname, "TEXT", TRUE, 5);
        $obj_email = FORM::GetObjInput("email", $this->user->email, "EMAIL", TRUE, 7);
        $obj_phone = FORM::GetObjInput("phone", $this->user->phone, "TEXT", NULL, 0);
        $obj_password = FORM::GetObjInput("password", NULL, "PASSWORD", TRUE, 5);
        $error_cnt += $obj_fullname->validate();
        $error_cnt += $obj_phone->validate();
        $error_cnt += $obj_password->validate();
        $error_cnt+=$obj_email->validate();
        if (isPostBack()) {
            // check password
            if ($this->user->password != md5($obj_password->value)) {
                $error_cnt ++;
                $obj_password->error = "Password incorrect";
                $obj_password->is_error = true;
            }
            if (! validate_email($obj_email->value)) {
                $error_cnt ++;
                $obj_email->is_error = true;
                $obj_email->error = "email not valid";
            }
            
            if (! $error_cnt) {
                UserModel::update_info($obj_fullname->value, $obj_email->value, $obj_phone->value, $this->user->user_id);
                header('Location:/setting/profile?success=true');
                exit();
            }
        }
       
        $frm->version = 4;
        $frm->create_h3("Profile Setting");
        $frm->create_obj_input(1, $obj_fullname,"Full Name");
        $frm->create_row($obj_fullname->error);
        $frm->create_obj_input(1, $obj_email,"Email");
        $frm->create_row($obj_email->error);
        $frm->create_obj_input(2, $obj_phone,"Phone");
        $frm->create_row($obj_phone->error);
        $frm->create_obj_input(1, $obj_password,"Your Password");
        $frm->create_row($obj_password->error);
        $this->vars->frm=$frm->generate();
        $this->_view('/backend/frm_user');
    }
    public function password()
    {
        $error_cnt = 0;
        $frm = new Form("frm_bank", "/setting/password", "POST");
        $obj_password1=FORM::GetObjInput("upassword", NULL, "PASSWORD",TRUE,6);
        $obj_password2=FORM::GetObjInput("upassword2", NULL, "PASSWORD",TRUE,6);
        $obj_password = FORM::GetObjInput("password", NULL, "PASSWORD", TRUE, 5);
        $error_cnt+=$obj_password2->validate();
        $error_cnt += $obj_password1->validate();
        $error_cnt += $obj_password->validate();
        if (isPostBack()) {
            // check password
            if ($this->user->password != md5($obj_password->value)) {
                $error_cnt ++;
                $obj_password->error = "Password incorrect";
                $obj_password->is_error = true;
            }
            if($obj_password1->value!=$obj_password2->value)
            {
                $error_cnt++;
                $obj_password1->error="New password not match";
                $obj_password1->is_error=true;
            }
            if (! $error_cnt) {
                UserModel::updatePassword($this->user->user_id, $obj_password1->value);
                header('Location:/dashboard/');
                exit();
            }
        }
        
        $frm->version = 4;
        $frm->create_h3("Password Setting");
        $frm->create_obj_input(1, $obj_password1,"New Password");
        $frm->create_obj_input(2, $obj_password2,"New Password Again");
        $frm->create_row($obj_password1->error);
        $frm->create_obj_input(1, $obj_password,"Your Password");
        $frm->create_row($obj_password->error);
        $this->vars->frm=$frm->generate();
        $this->_view('/backend/frm_user');
    }
}