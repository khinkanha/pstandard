<?php

class Dashboard extends UserController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
       $this->_view('/backend/index');
    }
    public function withdraw($page=1)
    {
        $page = intval($page);
        $limit = 25;
        $offset = 0;
        if ($page > 0) {
            $offset = ($page - 1) * $limit;
        }
        $obj_page = new Paging(WithdrawModel::get_total_record($this->user->user_id), $limit, $page);
        $obj_page->set_link("/dashboard/withdraw/");
        $this->vars->paging = $obj_page;
        $this->vars->_list = WithdrawModel::get_my_withdraw_list($this->user->user_id, $limit, $offset);
        $this->_view('/backend/mywithdrawlist');
    }
    public function downline($username = "")
    {
        $result = null;
        $user_id = $this->user->user_id;
        if ($username != "") {
            $a_user=UserModel::getUsername($username);
            if ($a_user) {
                $user_id = $a_user->user_id;
            }
        }
        $result = UserModel::get_downline($user_id);
        $this->vars->result = $result;
        $this->_view('/backend/downline');
    }
    public function switchback()
    {
        if(isset($_SESSION['admin_id']))
        {
            $_SESSION['user_id']=$_SESSION['admin_id'];
            header('Location: /dashboard/');
            exit();
        }
    }
}