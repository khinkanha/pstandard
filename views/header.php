<!DOCTYPE html>
<html lang="en">
<head>
<?php
global $title;
echo '<title>' . $title . '</title>';
?>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<style>
</style>
</head>
<body>
	<?php

echo '<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		<!-- Brand -->
		<a class="navbar-brand" href="#"><img src="/asset/logo.png" alt="T20 Real Estate"
			style="max-width: 100px;"></a>
    
		<!-- Links -->
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#collapsibleNavbar">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="collapsibleNavbar">
			<ul class="navbar-nav">';

if (isset($user)) {
    ?>
    <li class="nav-item <?php if($mytab=="home"){echo "active";} ?>"><a
		class="nav-link " href="/">Home</a></li>
 	
    <?php
    if ($is_admin) {

        ?><li
		class="nav-item dropdown <?php if($mytab=="admin"){echo "active";}?>">
		<a class="nav-link dropdown-toggle" href="#" id="navbardrop"
		data-toggle="dropdown">Admin Panel</a>
		<div class="dropdown-menu">
			<a class="dropdown-item" href="/admin/members/">Members</a>

		</div>
	</li>
	<li class="nav-item <?php if($mytab=="project"){echo "active";} ?>"><a
		href="/project/" class="nav-link">Project</a></li>
<?php
    }
}

echo '    </ul>
			<ul class="navbar-nav ml-auto">';
if (isset($user)) {
    ?>
    <li
		class="nav-item dropdown <?php if($mytab=="setting"){echo "active";}?>"><a
		class="nav-link dropdown-toggle" href="#" id="navbardrop"
		data-toggle="dropdown">
					<?php

    echo ucwords($user->fullname) . '</a>';
    echo '<div class="dropdown-menu dropdown-menu-right">
						<a class="dropdown-item" href="/setting/profile">Profile</a> <a
							class="dropdown-item" href="/setting/bank">Bank</a> <a
							class="dropdown-item" href="/setting/password">Password</a> <a
							class="dropdown-item" href="/logout">Log out</a>';
    if (isset($_SESSION['admin_id'])) {
        if (! $is_admin) {
            echo ' <a class="dropdown-item" href="/dashboard/switchback">Switch back</a>';
        }
    }
    echo '</div></li>';
} else {
    echo '<li class="nav-item">
      <a class="nav-link" href="/login">Login</a>
    </li>';
}

echo ' </ul>

		</div>
	</nav>';
if (isset($breadcrumb)) {
    echo $breadcrumb;
}

?>

	<div class="container" style="margin-top: 30px">