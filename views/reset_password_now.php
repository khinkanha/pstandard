<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/x-icon"
	href="/asset/logo.png">
<title><?php 
global $title;
echo $title;
?></title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<style type="text/css">
body,html {
   height: 100%;
    margin: 0;
}
.bg {
  /* The image used */
	background-image: url("/asset/background.jpg" );

  /* Full height */
  height: 100%; 

  /* Center and scale the image nicely */
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
}

.login-form {
	width: 340px;
	margin: 0px auto;
	color: #969fa4;
}

.login-form form {
	margin-bottom: 15px;
	background: #f7f7f7;
	box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
	padding: 30px;
	border-radius: 10px;
}

.login-form h2 {
	color: #636363;
	margin: 0 0 15px;
}

.form-control, .btn {
	min-height: 38px;
	border-radius: 2px;
	color: #969fa4;
}

.btn {
	font-size: 15px;
	font-weight: bold;
}
</style>
</head>
<body>


	<div class="container">
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
<?php
$login_frm = new Form ( 'reset_password', "/reset_password/".$post_username.'/'.$code, "POST" );
$login_frm->create_info ( 1, "info", "Reset Password" );
$login_frm->create_row ();
$login_frm->submit ( "cmdchange", "Setting", "Change", 1 );
$login_frm->create_input ( 1, "Password", "npassword", NULL, "New Password", 1, "Password", NULL );
$login_frm->create_input ( 2, "Password", "conpassword", NULL, "Confirm Password", 1, "Password", NULL );
$login_frm->create_row ();
$login_frm->create_input ( 2, "HIDDEN", "code", $code);
$login_frm->create_row();
$login_frm->create_input(1, "HIDDEN", "username", $post_username);
$login_frm->create_row();
echo '<div class="card"><div class="card-header bg-primary text-light">Change Password</div><div class="card-body">';
if (isset ( $error_msg )) {
    echo '<div class="alert alert-warning">
				<strong>Error!</strong> ' . $error_msg . '
				</div>
				';
}
if (isset($success_msg)) {
    echo '<div class="alert alert-success">
				<strong>Success!</strong> ' . $success_msg . '
				</div>';
}
echo $login_frm->generate ();

echo "</div></div></div>";


?>
</div>
			<div class="col-md-3"></div>
		</div>
	</div>


</body>
</html>