<?php
/**
 * Default ERROR Message
 */
?>
<!doctype html>
<html>
<head>
<meta charset=”UTF-8″>
<style>
body {
	background-color: #333;
}
#wrapper {
	position: relative;
	margin: 0 auto 0 auto;
}

header {

	height: 100px;
	background-color: #333;
	font-family: Arial, Helvetica, sans-serif;
	font-style: normal;
	font-size: 30px;
	padding: 50px 0 5px 25px;
	color: #FC0;
}

nav {
	height: 30px;
	background-color: #FC0;
	font-family: Tahoma, Geneva, sans-serif;
	font-weight: bold;
	font-size: 12px;
	color: #333;
	padding: 17px 0 3px 25px;
}

article {
	background-color: #333;
	font-family: Arial, Helvetica, sans-serif;
	font-style: normal;
	font-size: 12px;
	line-height: 21px;
	color: #CCC;
	padding: 25px 25px 5px 25px;
}

aside {
	background-color: #666;
	font-family: Arial, Helvetica, sans-serif;
	font-style: normal;
	font-size: 12px;
	color: #CCC;
	padding: 25px 15px 10px 15px;
	line-height: 16px;
}

h1, h2, h3 {
	font-family: Arial, Helvetica, sans-serif;
	line-height: 21px;
}

#footer {
	clear: both;
	height: 30px;
	background-color: #FC0;
	font-family: Arial, Helvetica, sans-serif;
	font-weight: normal;
	font-size: 12px;
	color: #333;
	padding: 20px 0 0 25px;
}

nav a {
	color: #000;
	text-decoration: none;
}

nav a:hover {
	color: #919191;
	text-decoration: underline;
}

a {
	color: #FC0;
	text-decoration: none;
}

a:hover {
	text-decoration: underline;
}

.t_images {

	width: 155px;
	border: 1px solid #999;
	margin: 0 15px 25px 15px;
	padding: 5px;
}
</style>
</head>

<body>
	<div id=”wrapper”>
		<header>
			<h1>
				Simple MVC <small>by In Mean</small>
			</h1>
		</header>
		
<?php
/* <nav>
 * <a href=”#”>Home</a>
 * <a href=”#”>About Us</a>
 * <a href=”#”>Services</a>
 * <a href=”#”>Contact</a>
 * </nav>
 */
?>
                  
		<article>
			<section id=”article1″>
				<h2>ERROR</h2>
				An error is triggered within this application, please inform website
				administration.
			</section>
		</article>
<?php 
//		<aside></aside>
?>
		<div id=”footer”></div>
	</div>
</body>
</html>