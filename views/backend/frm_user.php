<?php
require_once 'views/header.php';
echo '<div class="container">';
if(isset($_GET['success']))
{
    echo '<div class="alert alert-success alert-dismissible">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Success!</strong>
</div>';
}
echo $frm;
echo '</div>';
require_once 'views/footer.php';