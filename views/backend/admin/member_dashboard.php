<?php
include_once ('views/header.php');
echo '<div class="container">';
/**
 * @var UserModel $member
 */
echo '<h3>'.ucwords($member->username).' Dashboard</h3>';
echo '<div class="card"><div class="card-header h5 text-info">User Information <a href="/admin/user_profile/'.$member->username.'" ><i class="fa fa-pencil"></i>Edit</a></div>
<div class="card-body">
<ul class="list-group list-group-flush">
  <li class="list-group-item">Name <mark>'.$member->fullname.'</mark></li>
  <li class="list-group-item"><i class="fa fa-envelope"></i> '.$member->email.'</li>
  <li class="list-group-item"><i class="fa fa-phone"></i> '.$member->phone.'</li>
<li class="list-group-item"> <h3><span class="badge badge-success" title="Bonus">'.number_format($member->balance,2)." $".'</span>

</li>
</ul>
</div>
<div class="card-footer"><a href="/admin/force_log/'.$member->username.'" class="btn btn-danger">Force Login</a> <a href="/admin/password/'.$member->username.'" class="btn btn-info">Password</a></div>
</div><br/>';

echo '<div class="card"><div class="card-header h5 text-info">Bank Information <a href="/admin/user_bank/'.$member->username.'" ><i class="fa fa-pencil"></i> Edit</a></div>
<div class="card-body">
<ul class="list-group list-group-flush">
  <li class="list-group-item">Bank Name <mark>'.$member->bank_name.'</mark></li>
  <li class="list-group-item">Account Name <mark>'.$member->account_name.'</mark></li>
  <li class="list-group-item">Account Number <mark>'.$member->account_no.'</mark></li>
</ul>
</div></div>';
echo '<br/></div>';
include_once ('views/footer.php');