<?php
include_once ('views/header.php');
echo '<div class="container">';
if (isset($frm)) {
    echo $frm;
}
if(isset($frmsearch))
{
    echo $frmsearch;
}

$at = new Table();
$at->add_header("Username");
$at->add_header("Full Name");
$at->add_header("Email");
$at->add_header("Phone");
$at->add_header("Create date");
$at->add_row();
/**
 * @var UserModel $u
 */
foreach ($members as $u) {
    $at->add_col('<a href="/admin/member_dashboard/'.$u->username.'" class="text-info">'.$u->username.'</a>');
    $at->add_col($u->fullname);
    $at->add_col($u->email);
    $at->add_col($u->phone);
    $at->add_col(Helper::time_after($u->created_at));
    $at->add_row();
}
echo '<h5 class="text-success">Member List</h3>';
echo '<div class="text-right"><a href="/admin/create_user" class="btn btn-info">Create User</a></div>';
echo '<p>'.$paging.'</p>';
echo $at->generate();
echo $paging;
echo '<br/></div>';
include_once ('views/footer.php');