<?php
require_once 'views/header.php';
?>
<!-- For table -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" />
<link rel="stylesheet"
	href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css" />

<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script
	src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script
	src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script
	src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script
	src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<?php
echo '<div class="container">';
echo '<br/><div class="card"><div class="card-header h4 text-success bg-dark">Withdraw Batch Dashboard</div><div class="card-body h5">
  
 <div class="row">
    <div class="col-md-6">
    <p>Batch Created : <span class="text-info">' . $batch->created_at . '</span></p>
	<p>Total Payment : <span class="text-info">' . number_format($batch->total_amount_paid, 2) . ' $</span></p>
    <p>Batch Status : <span class="text-info">' . ucwords($batch->batch_status) . '</span></p></div>';
if ($batch->batch_status != WithdrawBatchModel::STATUS_COMPLETED) {
    $frm = new Form("frm_batch_status", "/admin/update_batch", "POST");
    $frm->version = 4;
    $frm->submit("cmd", "Update Batch", "Update", 1);
    $options = array(
        array(
            'created',
            'Created'
        ),
        array(
            'processing',
            'Processing'
        ),
        array(
            'completed',
            'Completed'
        )
    );
    $frm->create_select(1, $options, "batch_status", $batch->batch_status, "Change Batch Status");
    $frm->create_row();
    $frm->create_hidden("batch_id", $batch->batch_id);
    echo '<div class="col-md-6 bg-info">';
    echo $frm->generate();
    echo '</div>';
}
echo '
	</div>
    </div></div>';
echo '<h3>Withdraw List</h3>';
$tb = new Table();
$tb->add_header("No");
$tb->add_header("Amount");
$tb->add_header("Date");
$tb->add_header("Bank Name");
$tb->add_header("Account Name");
$tb->add_header("Account Number");
if ($col != FALSE) {
    $i = 1;
    foreach ($col as $obj) {
        $tb->add_col($i);
        $tb->add_col(number_format($obj->amount, 2) . ' $');
        $tb->add_col($obj->created_at);
        $tb->add_col($obj->bank_name);
        $tb->add_col($obj->account_name);
        $tb->add_col($obj->account_number);
        $tb->add_row();
        $i ++;
    }
}

echo $tb->generate();
echo '</div>';
require_once 'views/footer.php';
?>
<script>
$(document).ready(function(){
		var printCounter = 0;
	 
    // Append a caption to the table before the DataTables initialisation
    //$('.table').append('<caption style="caption-side: bottom">Cambodia Sim Card.</caption>');
    $('.table').DataTable( {
    	"pageLength": 50,
        dom: 'Bfrtip',
        buttons: [
        	
            {
                extend: 'excel',
                text:'Export',
                messageTop: 'The information in this table is copyright to Smart Global Plus.'
            }
        ]
    } );
	$("#DataTables_Table_0_filter").hide();
	$("#DataTables_Table_0_paginate").hide();
    $("#DataTables_Table_0_paginate").hide();
	
});
</script>
