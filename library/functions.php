<?php

function get_post($var_name, $html_safe = FALSE)
{
    if (isset($_POST[$var_name])) {
        if ($html_safe == TRUE) {
            return htmlspecialchars(trim($_POST[$var_name]));
        } else {
            return trim($_POST[$var_name]);
        }
    }
    return NULL;
}

/**
 * Is user is valid?
 *
 * @param string $username
 * @param number $min_lenght
 * @return boolean
 */
function validate_username($username, $min_lenght = 3)
{
    if (ctype_alnum($username)) {
        if (strlen($username) < $min_lenght) {
            return FALSE;
        } else {
            return TRUE;
        }
    }
    return FALSE;
}

function validate_email($email)
{
    if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
        return FALSE;
    }
    return TRUE;
}

/**
 * Make random code
 *
 * @param number $length
 * @return string
 */
function rnd_code($length = 8)
{
    $characters = '2345679ACDEFGHJKLMNPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < $length; $i ++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
}

function rnd_file_name($length = 8)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
    $randomString = '';
    for ($i = 0; $i < $length; $i ++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
}

function safe_coin($amount)
{
    $aa = round($amount, 6);
    return $aa;
}

/**
 * prepares a string optimized for SEO
 *
 * @see https://blog.ueffing.net/post/2016/03/14/string-seo-optimieren-creating-seo-friendly-url/
 * @param String $string
 * @return String $string SEO optimized String
 */
function seofy($sString = '')
{
    $sString = preg_replace('/[^\\pL\d_]+/u', '-', $sString);
    $sString = trim($sString, "-");
    $sString = iconv('utf-8', "us-ascii//TRANSLIT", $sString);
    $sString = strtolower($sString);
    $sString = preg_replace('/[^-a-z0-9_]+/', '', $sString);
    
    return $sString;
}

/**
 * Check if password strong
 *
 * @param string $pwd
 * @param number $lenght
 * @param string $errors
 * @return boolean
 */
function checkPassword($pwd, $lenght = 6, &$errors)
{
    $errors_init = $errors;
    
    if (strlen($pwd) < $lenght) {
        $errors[] = "At least " . $lenght . " chars long! ";
    }
    
    if (! preg_match("#[0-9]+#", $pwd)) {
        $errors[] = "At least one number! ";
    }
    
    if (! preg_match("#[a-zA-Z]+#", $pwd)) {
        $errors[] = "At least one letter! ";
    }
    
    return ($errors == $errors_init);
}

/**
 * Is postback?
 *
 * @return boolean
 */
function isPostBack()
{
    return ($_SERVER['REQUEST_METHOD'] == 'POST');
}

function secondtotime($seconds)
{
    if (! is_numeric($seconds))
        throw new Exception("Invalid Parameter Type!");
    
    $ret = "";
    
    $days = floor($seconds / 86400);
    $a_day = (string) $days;
    $seconds = $seconds - ($days * 86400);
    $hours = (string) floor($seconds / 3600);
    $seconds = $seconds - ($hours * 3600);
    $mins = (string) floor($seconds / 60);
    $secs = (string) $seconds % 60;
    
    if (strlen($a_day) == 1)
        $a_day = "0" . $a_day;
    if (strlen($hours) == 1)
        $hours = "0" . $hours;
    if (strlen($secs) == 1)
        $secs = "0" . $secs;
    if (strlen($mins) == 1)
        $mins = "0" . $mins;
    if ($days > 0) {
        if ($days == 1) {
            $ret = $a_day . " day ";
        } else {
            $ret = $a_day . " days ";
        }
    }
    if ($hours == 0)
        $ret .= "$mins:$secs";
    else
        $ret .= "$hours:$mins:$secs";
    
    return $ret;
}