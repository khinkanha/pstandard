<?php

class Core
{

    private $_url = null;

    private $_raw_url = null;

    private $_controller = null;

    private $_controllerPath = 'controllers/';

    // Always include trailing slash
    private $_subController = null;

    private $_errorFile = 'errorcontroller.php';

    private $_defaultFile = 'home.php';

    /**
     * Starts the Core
     *
     * @return boolean
     */
    public function init()
    {
        // Sets the protected $_url
        $this->_getUrl();
        
        // Looking for the 2nd level folder
        if (! empty($this->_url[0])) {
            $file = $this->_controllerPath . $this->_url[0];
            if (file_exists($file)) {
                // Mark it's a sub folder
                $this->_subController = $this->_url[0] . '/';
                // Remove the first item from the array
                array_splice($this->_url, 0, 1);
            }
        }
        
        // Load the default controller if no URL is set
        // eg: Visit http://localhost it loads Default Controller
        if (empty($this->_url[0])) {
            if ($this->_loadDefaultController()) {
                $this->_controller->index();
            }
            return false;
        }
        
        // Load existing or default controller
        if ($this->_loadExistingController()) {
            $this->_callControllerMethod();
        } else {
            // Check Method from default Controller
            if ($this->_loadDefaultController()) {
                $this->_callDefaultControllerMethod();
            }
        }
    }

    /**
     * (Optional) Set a custom path to controllers
     *
     * @param string $path
     */
    public function setControllerPath($path)
    {
        $this->_controllerPath = trim($path, '/') . '/';
    }

    /**
     * (Optional) Set a custom path to the error file
     *
     * @param string $path
     *            Use the file name of your controller, eg: error.php
     */
    public function setErrorFile($path)
    {
        $this->_errorFile = trim($path, '/');
    }

    /**
     * (Optional) Set a custom path to the error file
     *
     * @param string $path
     *            Use the file name of your controller, eg: index.php
     */
    public function setDefaultFile($path)
    {
        $this->_defaultFile = trim($path, '/');
    }

    /**
     * Fetches the $_GET from 'url'
     */
    private function _getUrl()
    {
        $url = isset($_GET['url']) ? $_GET['url'] : null;
        $url = rtrim($url, '/');
        $url = ltrim($url, '/');
        $url = filter_var($url, FILTER_SANITIZE_URL);
        $this->_raw_url = $url;
        $url = Router::apply($url);
        $this->_url = explode('/', $url);
    }

    /**
     * This loads if there is no GET parameter passed
     */
    private function _loadDefaultController()
    {
        $c_file = null;
        $e_file = $this->_controllerPath . 'errorcontroller.php';
        // Also load nested Home
        if (! empty($this->_subController)) {
            $c_file = $this->_controllerPath . $this->_subController . $this->_defaultFile;
        } else {
            $c_file = $this->_controllerPath . $this->_defaultFile;
        }
        
        if (file_exists($c_file)) {
            require_once ($c_file);
            $this->_controller = new Home();
            // $this->_controller->index ();
        } else {
            require_once ($e_file);
            $this->_controller = new ErrorController();
            $this->_controller->code($this->_defaultFile);
            return false;
        }
        return true;
    }

    /**
     * Load an existing controller if there IS a GET parameter passed
     *
     * @return boolean|string
     */
    private function _loadExistingController()
    {
        $file = null;
        $file2 = null;
        if (empty($this->_subController)) {
            $file = $this->_controllerPath . $this->_url[0] . '.php';
            $file2 = $this->_controllerPath . $this->_url[0] . 'Controller.php';
        } else {
            $file = $this->_controllerPath . $this->_subController . $this->_url[0] . '.php';
            $file2 = $this->_controllerPath . $this->_subController . $this->_url[0] . 'Controller.php';
        }
        if (file_exists($file2)) {
            require_once ($file2);
            $this->_controller = new $this->_url[0]($this->_url);
        } elseif (file_exists($file)) {
            require_once ($file);
            $this->_controller = new $this->_url[0]($this->_url);
        } else {
            // $this->_error ();
            return false;
        }
        return true;
    }

    /**
     * Call Default Controller additional methods
     */
    private function _callDefaultControllerMethod()
    {
        if (method_exists($this->_controller, $this->_url[0])) {
            $args = array_splice($this->_url, 1);
            $this->_controller->set_args($args);
            $r = new ReflectionMethod($this->_controller, $this->_url[0]);
            if (count($args) >= $r->getNumberOfRequiredParameters()) {
                call_user_func_array(array(
                    $this->_controller,
                    $this->_url[0]
                ), $args);
            } else {
                $this->_error();
                return false;
            }
        } else {
            $this->_error();
            return false;
        }
        return true;
    }

    /**
     * If a method is passed in the GET url paremter
     *
     * http://localhost/controller/method/(param)/(param)/(param)
     * url[0] = Controller
     * url[1] = Method
     * url[2] = Param
     * url[3] = Param
     * url[4] = Param
     */
    private function _callControllerMethod($method_param = 1)
    {
        $length = count($this->_url);
        
        // Make sure the method we are calling exists
        if ($length > 1) {
            if (method_exists($this->_controller, $this->_url[1])) {
                $args = array_splice($this->_url, 2);
                $this->_controller->set_args($args);
                $r = new ReflectionMethod($this->_controller, $this->_url[1]);
                if (count($args) >= $r->getNumberOfRequiredParameters()) {
                    try {
                        call_user_func_array(array(
                            $this->_controller,
                            $this->_url[1]
                        ), $args);
                    } catch (TypeError $e) {
                        $this->_error();
                        return false;
                    }
                } else {
                    $this->_error();
                    return false;
                }
            } else {
                $rr = new ReflectionMethod($this->_controller, 'index');
                $my_param_cnt = $rr->getNumberOfParameters();
                if (($my_param_cnt > 0) && ($my_param_cnt == ($length - 1))) {
                    $args = array_splice($this->_url, 1);
                    try {
                        call_user_func_array(array(
                            $this->_controller,
                            'index'
                        ), $args);
                    } catch (TypeError $e) {
                        $this->_error();
                        return false;
                    }
                } else {
                    $this->_error();
                    return false;
                }
            }
        } else {
            $this->_controller->index();
        }
        return true;
    }

    /**
     * Display an error page if nothing exists
     *
     * @return boolean
     */
    private function _error()
    {
        require_once ($this->_controllerPath . $this->_errorFile);
        $this->_controller = new ErrorController();
        $this->_controller->index();
        exit();
    }
}