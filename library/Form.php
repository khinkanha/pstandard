<?php
class Form {
	public $formName = NULL;
	public $formAction = NULL;
	public $formMethod = "POST";

	/**
	 * Bootstrap version 3, 4 by default 3
	 *
	 * @var integer
	 */
	public $version = 3;
	private $submitBtn = '<button type="submit" class="mb-2 btn btn-primary" name="submit"
						value="">Submit</button>';
	private $resetBtn = '';
	private $gpsBtn = '';
	private $signupBtn = '';
	private $linkBtn = '';
	private $submitBtn2 = '';
	private $row_create = 0;
	private $col_create = 0;
	private $all_row = NULL;
	private $col_text = NULL;
	private $input_str1 = NULL;
	private $input_str2 = NULL;
	private $pull_info_dom = "";
	private $pull_functions = "";
	private $_use_upload = FALSE;
	private $gps_js = '';
	private $g_recaptcha = NULL;

	/**
	 * Tell to create submit button at the end
	 *
	 * @var boolean
	 */
	public $inline_submit = FALSE;

	/**
	 * If inline submit has been called
	 *
	 * @var boolean
	 */
	private $inline_submit_used = FALSE;
	private $more_js = [ ];
	public $validated = FALSE;
	public $pull_auto_clear = '';

	/**
	 * Is panel is being used?
	 *
	 * @var string
	 */
	public $use_panel = FALSE;

	/**
	 * *
	 *
	 * @var integer <ul>
	 *      <li>1 = additional .00</li>
	 *      <li>2 = additional $$</li>
	 *      <li>3 = additional BTC</li>
	 *      <li>4 = additional BTC</li>
	 *      <li>5 = Month</li>
	 *      <li>6 = Custom char</li>
	 *      <li>7 = Prepend</li>
	 *      </ul>
	 */
	public $special = 0;
	public $specialchar = NULL;

	/**
	 * *
	 *
	 * @var integer <ul>
	 *      <li>0 = full</li>
	 *      <li>1 = half</li>
	 *      <li>2 = 1/3</li>
	 *      </ul>
	 */
	public $in_size = 0;
	public $no_auto_complete = FALSE;
	/**
	 * Create form
	 *
	 * @param string $formName
	 * @param string $formAction
	 * @param string $formMethod
	 * @param string $formStyle
	 * @param boolean $validated
	 */
	public function __construct($formName, $formAction, $formMethod, $formStyle = NULL, $validated = FALSE, $no_auto_complete = FALSE) {
		$this->formName = $formName;
		$this->formAction = $formAction;
		$this->formMethod = $formMethod;
		$this->validated = $validated;
		$this->no_auto_complete = $no_auto_complete;
	}

	/**
	 * Create a panel or card
	 *
	 * @param string $header_text
	 * @param string $color_mode
	 */
	public function create_panel($header_text = NULL, $color_mode = 'default') {
		$this->clear_panel ();
		if ($this->version == 3) {
			$this->all_row .= '<div class="panel panel-' . $color_mode . '">';
			if ($header_text) {
				$this->all_row .= '<div class="panel-heading">' . $header_text . '</div>';
			}
			$this->all_row .= '<div class="panel-body">';
		} else {
			$this->all_row .= '<div class="card mb-3 bg-' . $color_mode . '">';
			if ($header_text) {
				$this->all_row .= '<div class="card-header">' . $header_text . '</div>';
			}
			$this->all_row .= '<div class="card-body">';
		}
		$this->use_panel = TRUE;
	}

	/**
	 * Insert HTML or text into the form
	 *
	 * @param string $message
	 */
	public function create_text($message) {
		$this->all_row .= $message;
	}

	/**
	 * Clear the panel or card
	 */
	public function clear_panel() {
		if ($this->use_panel) {
			$this->use_panel = FALSE;
			$this->all_row .= "\t</div>\n</div>";
		}
	}

	/**
	 *
	 * @param string $bName
	 * @param string $Value
	 * @param string $bInfo
	 *        	- Display on the button
	 * @param number $bStyle
	 *        	<ul>
	 *        	<li> 0 - default</li>
	 *        	<li> 1 - primary</li>
	 *        	<li> 2 - success</li>
	 *        	</ul>
	 * @param boolean $disabled
	 *        	non-clickable button
	 * @param boolean $wider
	 *        	Make it 100% wide
	 */
	public function submit($bName, $Value, $bInfo = "Submit", $bStyle = 0, $disabled = FALSE, $wider = FALSE) {
		$this->submitBtn = '<button type="submit" class="mb-2 btn ';
		if ($bStyle == 0) {
			$this->submitBtn .= "btn-default";
		} elseif ($bStyle == 1) {
			$this->submitBtn .= "btn-primary";
		} elseif ($bStyle == 2) {
			$this->submitBtn .= "btn-success";
		}
		if ($wider) {
			$this->submitBtn .= " btn-lg btn-block";
		}

		$this->submitBtn .= '" name="' . $bName . '" value="' . $Value . '"';
		if ($disabled == TRUE) {
			$this->submitBtn .= ' disabled';
		}
		$this->submitBtn .= '>' . $bInfo . '</button>';
	}

	/**
	 *
	 * @param string $bName
	 * @param string $Value
	 * @param string $bInfo
	 * @param number $bStyle
	 *        	<ul>
	 *        	<li> 0 - default</li>
	 *        	<li> 1 - primary</li>
	 *        	<li> 2 - success</li>
	 *        	<li> 3 - danger</li>
	 *        	</ul>
	 */
	public function submit2($bName, $Value, $bInfo = "Submit", $bStyle = 0) {
		$this->submitBtn2 = '<button type="submit" class="btn ';
		if ($bStyle == 0) {
			$this->submitBtn2 .= "btn-default";
		} elseif ($bStyle == 1) {
			$this->submitBtn2 .= "btn-primary";
		} elseif ($bStyle == 2) {
			$this->submitBtn2 .= "btn-success";
		} elseif ($bStyle == 3) {
			$this->submitBtn2 .= "btn-danger";
		} elseif ($bStyle == 4) {
			$this->submitBtn2 .= "btn-warning";
		}

		$this->submitBtn2 .= '" name="' . $bName . '" value="' . $Value . '">' . $bInfo . '</button>';
	}
	public function reset() {
		$this->resetBtn = '<button type=\"reset\" class=\"btn btn-default\">Reset</button>';
	}

	// Create a new FORM Row to add element in
	public function create_row($row_status = 0) {
		$number_col = 0;
		$cur_row = NULL;

		if ($this->input_str1 != NULL || $this->input_str2 != NULL) {
			$number_col = 1;
			if (($this->input_str1 == NULL) && ($this->input_str2 != NULL)) {
				$this->input_str1 = $this->input_str2;
				$this->input_str2 = NULL;
			}
			if (($this->input_str1 != NULL) && ($this->input_str2 != NULL)) {
				$number_col = 2;
			}
		} else {
			$number_col = 0;
			trigger_error ( "Create empty row", E_USER_ERROR );
		}

		if ($number_col > 0) {

			if ($number_col == 2) {
				$cur_row .= "\n" . '<div class="row';
				if ($row_status == 1) {
					$cur_row .= " has-error has-feedback has-danger";
				}
				$cur_row .= '">';
				$cur_row .= "\n" . '<div class="col-sm-6">' . "\n";
				$cur_row .= $this->input_str1;
				$cur_row .= '</div>' . "\n";
				$cur_row .= "\n" . '<div class="col-sm-6">' . "\n";
				$cur_row .= $this->input_str2;
				$cur_row .= '</div>' . "\n";
				$cur_row .= '</div>';
			} else {
				$cur_row .= "\n<div class=\"in-row";
				if ($row_status == 1) {
					$cur_row .= " has-error has-feedback has-danger";
				}
				$cur_row .= "\">";
				$cur_row .= $this->input_str1;
				$cur_row .= "</div>";
			}
			$cur_row = "<div class=\"form-group\">\n" . $cur_row . "\n</div>\n";
			$this->all_row .= $cur_row;
			$this->input_str1 = NULL;
			$this->input_str2 = NULL;
		}
	}

	/**
	 * Create Header 1 divider
	 *
	 * @param string $message
	 * @param string $extra_class
	 */
	public function create_h1($message, $extra_class = NULL) {
		$this->all_row .= '<h1';
		if ($extra_class) {
			$this->all_row .= ' class="' . $extra_class . '"';
		}
		$this->all_row .= '>' . $message . "</h1>\n";
	}

	/**
	 * Create Header 2 divider
	 *
	 * @param string $message
	 * @param string $extra_class
	 */
	public function create_h2($message, $extra_class = NULL) {
		$this->all_row .= '<h2';
		if ($extra_class) {
			$this->all_row .= ' class="' . $extra_class . '"';
		}
		$this->all_row .= '>' . $message . "</h2>\n";
	}

	/**
	 * Create Header 3 divider
	 *
	 * @param string $message
	 * @param string $extra_class
	 */
	public function create_h3($message, $extra_class = NULL) {
		$this->all_row .= '<h3';
		if ($extra_class) {
			$this->all_row .= ' class="' . $extra_class . '"';
		}
		$this->all_row .= '>' . $message . "</h3>\n";
	}

	/**
	 * Create hidden field
	 *
	 * @param string $iName
	 * @param string $iValue
	 */
	public function create_hidden($iName, $iValue) {
		$this->all_row .= '<input type="HIDDEN" name="' . $iName . '" value="' . $iValue . '">' . "\n";
	}
	public function create_radio($options, $iName, $iValue, $iLabel) {
		if (is_array ( $options )) {
			// @todo - Check type of key array is it Number or String as key

			// @todo - Layout
			$this->input_str1 .= '<div class="form-group">';
			if (strlen ( $iLabel ) > 0) {
				$this->input_str1 .= "<label class=\"radio-inline\">$iLabel\n";
				$this->input_str1 .= '</label"> ' . "\n";
			}
			$i = 0;

			foreach ( $options as $a_option ) {
				$i = $i + 1;
				$this->input_str1 .= "<label class=\"radio-inline\">\n";
				$this->input_str1 .= "<input type=\"radio\" name=\"$iName\" id=\"inlineRadio" . $i . "\" value=\"" . $a_option [0] . "\" ";
				if ($a_option [0] == $iValue) {
					$this->input_str1 .= "checked";
				}
				$this->input_str1 .= ">" . $a_option [1] . "\n";
				$this->input_str1 .= '</label"> ' . "\n";
			}
			$this->input_str1 .= '</div>';
		}
	}

	/**
	 * Create a checkbox
	 *
	 * @param string $iName
	 * @param string $iValue
	 * @param string $iLabel
	 * @param string $inValue
	 *        	If matched with iValue it's will marked the box checked
	 * @param boolean $disabled
	 *        	If the checkbox is disabled
	 * @param boolean $inline
	 *        	If checkbox is order inline (same row)
	 * @param boolean $required
	 */
	public function create_checkbox($iName, $iValue, $iLabel, $inValue = NULL, $disabled = FALSE, $inline = FALSE, $required = FALSE) {
		$a_str = NULL;
		if ($inline == FALSE) {
			$a_str = "<div class=\"checkbox form-check mb-2\">";
		} else {
			$a_str = "<div class=\"checkbox form-check-inline mb-2\">";
		}
		$a_str .= "<label class=\"form-check-label\">";
		$a_str .= "<input type=\"checkbox\" class=\"form-check-input\" name=\"" . $iName . "\" value=\"" . $iValue . "\"";
		if ($inValue == $iValue) {
			$a_str .= " checked";
		}
		if ($disabled == TRUE) {
			$a_str .= " disabled";
		}
		if ($required) {
			$a_str .= " required";
		}
		$a_str .= ">";
		$a_str .= $iLabel;
		$a_str .= "</label>";
		$a_str .= "</div>";
		// $a_str .= "</div>";
		if (strlen ( $this->input_str1 ) == 0) {
			$this->input_str1 = $a_str;
		} else {
			$this->input_str2 .= $a_str;
		}
	}

	/**
	 * Create select input
	 *
	 * @param number $col_num
	 * @param array $options
	 *        	in array(array(value,display), array(value, display), ...)
	 * @param string $iName
	 * @param string $iValue
	 * @param string $iLabel
	 * @param boolean $auto_submit
	 *        	is form should be auto submit on value selected?
	 * @param boolean $required
	 * @param string $error_str
	 */
	public function create_select($col_num, $options, $iName, $iValue, $iLabel, $auto_submit = FALSE, $required = FALSE, $error_str = NULL) {
		$build_str = "<label for=\"" . $iName . "\">" . $iLabel . "</label>\n";
		if ($this->inline_submit) {
			$build_str .= "<div class=\"input-group\">\n";
		}
		$build_str .= "<select id=\"" . $iName . "\" name=\"" . $iName . "\" class=\"form-control custom-select\"";
		// If this form is auto submit
		if ($auto_submit) {
			$build_str .= " onChange=\"this.form.submit()\"";
		}
		if ($required) {
			$build_str .= 'required';
		}
		$build_str .= ">\n";

		foreach ( $options as $a_option ) {
			$build_str .= "\t<option value=\"" . $a_option [0] . "\"";
			if ($iValue == $a_option [0]) {
				$build_str .= " selected";
			}
			$build_str .= ">" . $a_option [1] . "</option>\n";
		}
		$build_str .= "</select>\n";
		if ($this->inline_submit) {
			$build_str .= '<div class="input-group-append">' . $this->submitBtn . '</div>';
			$this->inline_submit = FALSE;
			$this->inline_submit_used = TRUE;
			$build_str .= "</div>\n";
		}
//		$build_str = '<div class="form-group">' . $build_str;
		if ($error_str) {
			$build_str .= "\n\t\t" . '<div class="invalid-feedback">' . $error_str . "</div>\n\t";
		}
//		$build_str .= "</div>\t";
		if ($col_num == 1) {
			$this->input_str1 = $build_str;
		} else {
			$this->input_str2 = $build_str;
		}
	}
	public function create_inline_submit($col_num) {
		if ($col_num == 1) {
			$this->inline_submit_used = TRUE;
			$this->input_str1 = $this->submitBtn;
		} else {
			$this->inline_submit_used = TRUE;
			$this->input_str2 = $this->submitBtn;
		}
	}

	/**
	 * Build Input form
	 *
	 * @internal
	 *
	 * @param string $iType
	 *        	<ul>
	 *        	<li>Text - Input text</li>
	 *        	<li>Password - Input as password</li>
	 *        	</ul>
	 * @param string $iName
	 * @param string $iValue
	 * @param string $iLabel
	 * @param number $iRequired
	 * @param string $iPlaceholder
	 * @param number $iWarning
	 * @param string $extra
	 * @param string $error_text
	 * @param string $extra_class
	 * @see Form::create_input()
	 */
	private function build_input($iType = "TEXT", $iName, $iValue, $iLabel = NULL, $iRequired = 0, $iPlaceholder = NULL, $iWarning = 0, $extra = NULL, $error_text = NULL, $extra_class = NULL) {
		$input_str = "";

		if ($iLabel != NULL) {
			$input_str .= '<label for="' . $iName . '" class="control-label">' . $iLabel . '</label> ';
		}
		if (($this->special > 0) || ($this->inline_submit)) {
			$input_str .= "<div class=\"input-group\">\n";
		}
		if ($this->special == 2) {
			if ($this->version == 4) {
				$input_str .= "<div class=\"input-group-prepend\"><span class=\"input-group-text\">$</span></div>";
			} else {
				$input_str .= "<span class=\"input-group-addon\">$</span>\n";
			}
		}
		$aria_desc = NULL;
		if ($this->special == 7) {
			$aria_desc = "inputGroupPrepend" . rand ( 1, 1000 );
			$input_str .= "<div class=\"input-group-prepend\"><span class=\"input-group-text\" id=\"" . $aria_desc . "\">" . $this->specialchar . "</span></div>";
		}

		$input_str .= '<input type="' . $iType . '" class="form-control';
		if ($iWarning > 0) {
			$input_str .= ' is-invalid form-control-danger';
		}
		if ($extra_class) {
			$input_str .= ' ' . $extra_class;
		}
		$input_str .= '" id="' . $iName . '" name="' . $iName . '"';
		if ($iPlaceholder != NULL) {
			$input_str .= ' placeholder="' . $iPlaceholder . '" ';
		}
		if ($iValue !== NULL) {
			$input_str .= ' value="' . $iValue . '"';
		}
		if ($iRequired == 1) {
			$input_str .= ' required';
		}
		if ($extra != NULL) {
			$input_str .= ' ' . $extra . ' ';
		}
		if ($this->special == 7) {
			$input_str .= ' aria-describedby="' . $aria_desc . '"';
		}
		// End input
		$input_str .= '>' . "\n";
		if ($this->special == 1) {
			if ($this->version == 4) {
				$input_str .= "<div class=\"input-group-append\"><span class=\"input-group-text\">.00</span></div>";
			} else {
				$input_str .= "<span class=\"input-group-addon\">.00</span>";
			}
		}
		if ($this->special == 2) {
			if ($this->version == 4) {
				$input_str .= "<div class=\"input-group-append\"><span class=\"input-group-text\">.00</span></div>";
			} else {
				$input_str .= "<span class=\"input-group-addon\">.00</span>";
			}
		}
		if ($this->special == 3) {
			if ($this->version == 4) {
				$input_str .= "<div class=\"input-group-append\"><span class=\"input-group-text\">%</span></div>";
			} else {
				$input_str .= "<span class=\"input-group-addon\">%</span>";
			}
		}
		if ($this->special == 4) {
			if ($this->version == 4) {
				$input_str .= "<div class=\"input-group-append\"><span class=\"input-group-text glyphicon glyphicon-bitcoin\"></span></div>";
			} else {
				$input_str .= "<span class=\"input-group-addon\"><span class=\"glyphicon glyphicon-bitcoin\"></span></span>";
			}
		}
		if ($this->special == 5) {
			if ($this->version == 4) {
				$input_str .= "<div class=\"input-group-append\"><span class=\"input-group-text\">Month</span></div>";
			} else {
				$input_str .= "<span class=\"input-group-addon\">Month</span>";
			}
		}
		if ($this->special == 6) {
			if ($this->version == 4) {
				$input_str .= "<div class=\"input-group-append\"><span class=\"input-group-text\">" . $this->specialchar . "</span></div>";
			} else {
				$input_str .= "<span class=\"input-group-addon\">" . $this->specialchar . "</span>";
			}
		}
		if ($this->inline_submit) {
			$this->inline_submit_used = TRUE;
			$input_str .= '<div class="input-group-append">' . $this->submitBtn . '</div>';
		}
		if ($iWarning > 0) {
			if ($this->version == 3) {
				$input_str .= '<span class="fa fa-warning form-control-feedback"></span>';
				if ($error_text) {
					$input_str .= '<div class="help-block">' . $error_text . '</div>';
				}
			}
			if (($this->version == 4) && ($error_text != NULL)) {
				$input_str .= '<div class="invalid-feedback">' . $error_text . '</div>';
			}
		}
		if (($this->special > 0) || ($this->inline_submit)) {
			$input_str .= "</div>\n";
		}

		$this->special = 0;
		$this->inline_submit = FALSE;
		return $input_str;
	}

	/**
	 *
	 * @param number $col_num
	 *        	<p>Maxinum only 2 colume</p>
	 *        	<ol>
	 *        	<li>Colume 1</li>
	 *        	<li>Colume 2</li>
	 *        	</ol>
	 * @param string $iType
	 *        	<ul>
	 *        	<li>Text - Input text</li>
	 *        	<li>Password - Input as password</li>
	 *        	<li>Number - Input number only</li>
	 *        	</ul>
	 * @param string $iName
	 * @param string $iValue
	 * @param string $iLabel
	 * @param number $iRequired
	 * @param string $iPlaceholder
	 * @param number $iWarning
	 * @see Form::build_input()
	 */
	public function create_input($col_num, $iType, $iName, $iValue, $iLabel = NULL, $iRequired = 0, $iPlaceholder = NULL, $iWarning = 0, $extra = NULL, $error_text = NULL, $extra_class = NULL) {
		if ($iType == NULL) {
			$iType == "text";
		}
		if ($col_num == 1) {
			$this->input_str1 = $this->build_input ( $iType, $iName, $iValue, $iLabel, $iRequired, $iPlaceholder, $iWarning, $extra, $error_text, $extra_class );
		} else {
			$this->input_str2 = $this->build_input ( $iType, $iName, $iValue, $iLabel, $iRequired, $iPlaceholder, $iWarning, $extra, $error_text, $extra_class );
		}
	}

	/**
	 * Build input from object
	 *
	 * @param number $col_num
	 * @param FormObjTEXTInput $obj
	 * @param string $iLabel
	 * @param string $iPlaceholder
	 * @param string $extra
	 */
	public function create_obj_input($col_num, $obj, $iLabel = NULL, $iPlaceholder = NULL, $extra = NULL, $extra_class = NULL) {
		if (($obj->type == 'NUMBER') || ($obj->type == 'RANGE')) {
			if ($obj->char_min) {
				$extra .= ' min = "' . $obj->char_min . '"';
			}
			if ($obj->char_max) {
				$extra .= ' max = "' . $obj->char_max . '"';
			}
			$extra .= ' step = "any"';
		} else {
			if ($obj->char_max) {
				$extra .= ' maxlength = "' . $obj->char_max . '"';
			}
		}
		if ($col_num == 1) {
			$this->input_str1 = $this->build_input ( $obj->type, $obj->name, $obj->value, $iLabel, $obj->required, $iPlaceholder, $obj->is_error, $extra, $obj->error, $extra_class );
		} else {
			$this->input_str2 = $this->build_input ( $obj->type, $obj->name, $obj->value, $iLabel, $obj->required, $iPlaceholder, $obj->is_error, $extra, $obj->error, $extra_class );
		}
	}
	public function create_textarea($col_num, $iName, $iRow = 5, $iValue, $iLabel = NULL, $iRequired = 0, $iPlaceholder = NULL, $iWarning) {
		// <textarea rows="3"></textarea>
		$tmp_str = '<div class="form-group">';
		if ($iLabel != NULL) {
			$tmp_str .= '<label for="' . $iName . '" class="control-label">' . $iLabel . '</label> ';
		}
		$tmp_str .= '<textarea name="' . $iName . '" id="' . $iName . '"';
		if ($iPlaceholder != NULL) {
			$tmp_str .= ' placeholder="' . $iPlaceholder . '"';
		}
		$tmp_str .= ' rows="' . $iRow . '" class="form-control">';
		$tmp_str .= htmlentities ( $iValue );
		$tmp_str .= '</textarea>';
		$tmp_str .= '</div>';
		if ($col_num == 1) {
			$this->input_str1 = $tmp_str;
		} else {
			$this->input_str2 = $tmp_str;
		}
	}
	public function create_file($col_num, $iName, $iLabel = NULL, $iRequired = 0) {
		$input_str = "<div class=\"form-group\">\n";
		if ($iLabel != NULL) {
			$input_str .= '<label for="' . $iName . '" class="control-label">' . $iLabel . '</label> ';
		}
		$input_str .= '<input type="file" class="form-control" id="' . $iName . '" name="' . $iName . '"';
		if ($iRequired == 1) {
			$input_str .= " required";
		}
		$input_str .= '>';
		$input_str .= "</div>";
		$this->_use_upload = TRUE;
		if ($col_num == 1) {
			$this->input_str1 = $input_str;
		} else {
			$this->input_str2 = $input_str;
		}
		// <input type="file" name="fileToUpload" id="fileToUpload">
	}
	public function generate() {
		$this->clear_panel ();
		$frm_txt = '<form role="form" id="' . $this->formName . '"
			action="' . $this->formAction . '"
			method="' . $this->formMethod . '"';
		if ($this->no_auto_complete) {
			$frm_txt .= ' autocomplete="off"';
		}
		if ($this->_use_upload == TRUE) {
			$frm_txt .= ' enctype="multipart/form-data"';
		}
		if ($this->validated) {
			$frm_txt .= ' class="was-validated"';
		}
		$frm_txt .= '>' . "\n";
		$frm_txt .= $this->all_row;
		if ($this->g_recaptcha != NULL) {
			$frm_txt .= '<div class="in-row">' . "\n" . '<div class="form-group"><div class="g-recaptcha" data-sitekey="' . $this->g_recaptcha . '"></div>' . "\n</div>\n</div>";
		}
		if (! $this->inline_submit_used) {
			$frm_txt .= $this->submitBtn . "\n";
		}
		$frm_txt .= $this->submitBtn2 . "\n" . $this->resetBtn . "\n" . $this->gpsBtn . "\n" . $this->signupBtn . $this->linkBtn;
		$frm_txt .= '</form>' . "\n";
		return $frm_txt;
	}

	/**
	 * Create Empty label
	 *
	 * @see Form::pull_info()
	 * @param integer $col_num
	 * @param string $id_name
	 * @param string $default_val
	 *
	 */
	public function create_info($col_num, $id_name, $default_val, $iLabel = NULL) {
		$input_str = '';
		$input_str .= '<div class="form-group" >';
		if ($iLabel != NULL) {
			$input_str .= '<label for="' . $id_name . '" class="control-label">' . $iLabel . '</label> ';
		}
		$input_str .= "\n<span id=\"" . $id_name . "\" class=\"form-control\" readonly>" . $default_val . "</span>\n";
		$input_str .= '</div>';
		if ($col_num == 1) {
			$this->input_str1 = $input_str;
		} else {
			$this->input_str2 = $input_str;
		}
	}

	/**
	 * Pull Information from an INPUT to display in a Label, on key up on a given ID, data will pull from pull_info.php and display in an ID
	 *
	 * @see Form::create_info()
	 * @param string $from_id
	 * @param string $to_id
	 * @param string $pre_fix_result
	 */
	public function pull_info($from_id, $to_id, $pre_fix_result = NULL) {
		// @todo - Validation
		$this->pull_info_dom .= "\t$('#" . $from_id . "').keyup(function(){ pull_" . $from_id . "(); });\n";
		$this->pull_functions .= "\nfunction pull_" . $from_id . "()\n{\n\t";
		$this->pull_functions .= "var getGID = $('#" . $from_id . "').val();\n\t";
		$this->pull_functions .= "$.get('pull_info.php',{gid: getGID, cmd: '" . $from_id . "'})\n\t";
		$this->pull_functions .= ".done(function(data) {\n\t\t";
		$this->pull_functions .= "$('#" . $to_id . "').html('" . $pre_fix_result . "' + data);\n\t";
		if (strlen ( $this->pull_auto_clear ) > 0) {
			$this->pull_functions .= "\t$('#" . $this->pull_auto_clear . "').val(0);\n\t";
			$this->pull_auto_clear = '';
		}
		$this->pull_functions .= "});\n}\n";
	}

	/**
	 * Pull Information from AJAX_URL and update to the to_id
	 *
	 * @param string $from_id
	 * @param string $to_id
	 * @param string $ajax_url
	 * @param array $keys
	 *        	format in array(array(key,post), array(key,post), ...)
	 */
	public function pull_info2($from_id, $to_id, $ajax_url, $keys = []) {
		// @todo - Validation
		$this->pull_info_dom .= "\t$('#" . $from_id . "').keyup(function(){ pull_" . $from_id . "(); });\n";
		$this->pull_info_dom .= "\t$('#" . $from_id . "').change(function(){ pull_" . $from_id . "(); });\n";
		$this->pull_functions .= "\nfunction pull_" . $from_id . "()\n{\n\t";
		$this->pull_functions .= "var getGID = $('#" . $from_id . "').val();\n\t";
		if (strlen ( $this->pull_auto_clear ) > 0) {
			$this->pull_functions .= "\t$('#" . $this->pull_auto_clear . "').val(0);\n\t";
			$this->pull_auto_clear = '';
		}
		$this->pull_functions .= "$.ajax({
				url: '" . $ajax_url . "',
				method: 'POST',
				dataType: 'json',
				data: {
					an_id: getGID";
		if (count ( $keys )) {
			foreach ( $keys as $key ) {
				$this->pull_functions .= ",\n\t\t\t\t\t" . $key [0] . ": " . $key [1] . "\n";
			}
		}
		$this->pull_functions .= "\n				},
				success: function(json) {
						if (json.success) { \n";
		$this->pull_functions .= "\t\t\t\t\t\t\t$('#" . $to_id . "').html(json.data);\n\t";
		$this->pull_functions .= "\t\t\t\t\t} else {
							$('#" . $to_id . "').html('');
                    }
                }
			})\n }";
	}

	/**
	 * Generate JS Content to use in Footer or Header
	 *
	 * @return string|NULL
	 */
	public function generate_js() {
		$dom_part = NULL;
		if (strlen ( $this->pull_info_dom ) > 0) {
			$dom_part .= "$(document).ready(function (){\n";
			$dom_part .= $this->pull_info_dom;
			$dom_part .= "});\n";
			$dom_part .= $this->pull_functions;
		}
		if (strlen ( $this->gps_js ) > 0) {
			$dom_part .= "\n" . $this->gps_js;
		}
		if (count ( $this->more_js )) {
			foreach ( $this->more_js as $js ) {
				$dom_part .= "\n" . $js;
			}
		}
		return $dom_part;
	}
	public function signup_button($a_link) {
		$this->signupBtn = '<a href="' . $a_link . '" class="btn btn-info">Sign Up</a>';
	}
	public function link_button($label, $a_link) {
		$this->linkBtn = '<a href="' . $a_link . '" class="btn btn-info">' . $label . '</a>';
	}
	public function gps_button($display, $in_latt, $in_long) {
		$this->gpsBtn = '<button type="button" class="btn btn-primary" name="gps"
						value="" onclick="getLocation()">' . $display . '</button>';
		$this->gps_js = 'var x = document.getElementById("formName");
						function getLocation() {
						    if (navigator.geolocation) {
						        navigator.geolocation.watchPosition(showPosition);
						    } else {
						        alert("Geolocation is not supported by this browser.");
							}
						}
				
						function showPosition(position) {
						    document.getElementById("' . $in_latt . '").value = position.coords.latitude;
						    document.getElementById("' . $in_long . '").value = position.coords.longitude;
						}
				';
	}

	/**
	 * Set recaptcha key
	 *
	 * @param string $captcha_key
	 */
	public function set_g_recaptcha($captcha_key) {
		$this->g_recaptcha = $captcha_key;
	}

	/**
	 * Generate additional header script to support advance script
	 *
	 * @return string|NULL
	 */
	public function generate_header() {
		if ($this->g_recaptcha != NULL) {
			return "<script src=\"https://www.google.com/recaptcha/api.js\"></script>\n";
		}
		return NULL;
	}
	public function create_input_list($col_num, $options, $iName, $iValue, $iLabel) {
		$build_str = "<label for=\"" . $iName . "\">" . $iLabel . "</label>\n";
		$build_str .= "<input type=\"text\" id=\"" . $iName . "\" name=\"" . $iName . "\" class=\"form-control\" list=\"data" . $iName . "\" value=\"" . $iValue . "\"/>\n";
		$build_str .= "<datalist name=\"data" . $iName . "\"id=\"data" . $iName . "\">";
		foreach ( $options as $a_option ) {
			$build_str .= "\t<option value=\"" . $a_option [0] . "\">\n";
		}
		$build_str .= "</datalist>";
		$build_str = '<div class="form-group">' . $build_str . '</div>';
		if ($col_num == 1) {

			$this->input_str1 = $build_str;
		} else {
			$this->input_str2 = $build_str;
		}
	}

	/**
	 * Add auto complete to input, required JQUERY UI
	 *
	 * @param string $for_name
	 * @param string $input_set
	 * @param number $char_count
	 * @param string $external_url
	 *        	JSON Address that return the array
	 */
	public function set_autocomplete($for_name, $input_set, $char_count = 2, $external_url = NULL) {
		$js_str = "\n\tvar " . $for_name . "_array = [];\n\t";
		$js_str .= '$( function() {' . "\n\t";
		$autoc = "\n\t\t" . '$("#' . $for_name . '").autocomplete({
            source: ' . $for_name . '_array,
            minLength:' . $char_count . '
        });';
		if ($external_url) {
			$js_str .= "\t$(document).ready(function () {";
			$js_str .= "\n\t\t$.get({
\t\t\turl: location.origin + \"" . $external_url . "\",
\t\t\tsuccess: function(result) { iDependOnMy" . $for_name . "(JSON.parse(result));}, \n\t\t\terror: function(result) {console.log(result); }";
			$js_str .= "\n\t\t});\n\t}); \n";
			$js_str .= "\tfunction iDependOnMy" . $for_name . "(param) { \n\t\t" . $for_name . "_array = param; " . $autoc . "}";
		} else {
			$aa = json_encode ( $input_set );
			$js_str .= $for_name . '_array = ' . $aa . ";\n";
			$js_str .= autoc;
		}

		$js_str .= "\n\t})\n\n";
		$js_str .= "\nconsole.log(window.lotno_array);\n";
		array_push ( $this->more_js, $js_str );
	}

	/**
	 * Create Input object
	 *
	 * @param string $name
	 * @param string $value
	 * @param string $type
	 * @param string $required
	 * @param number $char_min
	 * @param number $char_max
	 * @return FormObjTEXTInput
	 */
	static public function GetObjInput($name, $value, $type, $required = TRUE, $char_min = 0, $char_max = 0) {
		$obj = new FormObjTEXTInput ( $name, $value, $type, $required, $char_min, $char_max );
		return $obj;
	}
}
class InputImageUpload {
	public $uploadDirectory = NULL;

	/**
	 * Store all foreseen and unforseen errors here
	 *
	 * @var array
	 */
	public $errors = [ ];

	/**
	 * Get all the file extensions
	 *
	 * @var array
	 */
	public $fileExtensions = [ 
			'jpeg',
			'jpg',
			'png'
	];
	public $fileName;
	public $fileSize;
	public $fileTmpName;
	public $fileType;
	public $fileExtension;
	private $inputName;

	/**
	 * Create object
	 *
	 * @param string $inputName
	 */
	public function __construct($inputName) {
		$this->inputName = $inputName;
		if ($this->isPostBack ()) {
			if (isset ( $_FILES [$inputName] )) {
				$this->fileName = $_FILES [$inputName] ['name'];
				$this->fileSize = $_FILES [$inputName] ['size'];
				$this->fileTmpName = $_FILES [$inputName] ['tmp_name'];
				$this->fileType = $_FILES [$inputName] ['type'];
				$tmp = explode ( '.', $this->fileName );
				$this->fileExtension = strtolower ( end ( $tmp ) );
			}
		}
	}

	/**
	 * Is form had postback
	 *
	 * @return boolean
	 */
	private function isPostBack() {
		return ($_SERVER ['REQUEST_METHOD'] == 'POST');
	}
	public function upload_to($to_path, $file_name = NULL) {
		$currentDir = getcwd ();
		// $currentDir = NULL;
		$uploadPath = NULL;
		if ($to_path [strlen ( $to_path ) - 1] != '/') {
			$to_path .= '/';
		}
		if ($file_name == NULL) {
			$uploadPath = $currentDir . '/' . $to_path . basename ( $this->fileName );
		} else {
			$uploadPath = $currentDir . '/' . $to_path . basename ( $file_name ) . '.' . $this->fileExtension;
			$this->fileName = $file_name . '.' . $this->fileExtension;
		}
		$didUpload = move_uploaded_file ( $this->fileTmpName, $uploadPath );
		if ($didUpload) {
			return TRUE;
		}
		return FALSE;
	}
}

/**
 * Handling Object Input Property
 *
 * @author User
 *        
 */
class FormObjTEXTInput {
	public $name = NULL;

	/**
	 * Value from input
	 *
	 * @var string|integer|float
	 */
	public $value;
	public $type = NULL;

	/**
	 * Raw value only work for password
	 *
	 * @var string
	 */
	public $post_back_value = NULL;
	public $required = FALSE;
	public $error_array = [ ];

	/**
	 * Error string
	 *
	 * @var string
	 */
	public $error = NULL;

	/**
	 * Min value or minimum characters
	 *
	 * @var integer
	 */
	public $char_min = 0;

	/**
	 * Maxinum value or maximum characters
	 *
	 * @var integer
	 */
	public $char_max = 0;
	public $is_error = FALSE;
	public $value_len = 0;

	/**
	 * Create new object
	 *
	 * @param string $name
	 * @param string $value
	 * @param string $type
	 * @param boolean $required
	 * @param number $char_min
	 * @param number $char_max
	 */
	public function __construct($name, $value, $type, $required = FALSE, $char_min = 0, $char_max = 0) {
		$this->name = $name;
		$this->value = $value;
		$this->type = strtoupper ( $type );
		$this->required = $required;
		$this->char_min = $char_min;
		$this->char_max = $char_max;
	}

	/**
	 * Validation
	 *
	 * @return boolean
	 */
	public function validate() {
		if (isPostBack ()) {
			$str = NULL;
			$error_count = 0;
			if ($this->type == "PASSWORD") {
				$str = get_post ( $this->name );
				$this->post_back_value = $str;
				$this->value = $str;
			} else {
				$str = get_post ( $this->name, TRUE );
				if ($this->type == 'NUMBER') {
					$this->value = floatval ( $str );
				} else {
					$this->value = $str;
				}
			}
			$this->value_len = strlen ( $str );
			if ($this->required && ($this->value_len == 0)) {
				$error_count ++;
				$this->is_error = TRUE;
				$this->error_array [$error_count] = 'Input is required';
				$this->error = 'Input is required';
			}
			if ($this->type == 'NUMBER') {
				if ($this->char_min) {
					if ($this->value < $this->char_min) {
						$error_count ++;
						$this->is_error = TRUE;
						$this->error = 'Minimum value is ' . $this->char_min;
						$this->error_array [$error_count] = $this->error;
					}
				}
				if ($this->char_max) {
					if ($this->value > $this->char_max) {
						$error_count ++;
						$this->is_error = TRUE;
						$this->error = 'Maximum value is ' . $this->char_max;
						$this->error_array [$error_count] = $this->error;
					}
				}
			} else {
				if ($this->char_min > 0) {
					if ($this->value_len < $this->char_min) {
						$error_count ++;
						$this->is_error = TRUE;
						$this->error = 'Required at least ' . $this->char_min . ' characters';
						$this->error_array [$error_count] = $this->error;
					}
				}
				if ($this->char_max > 0) {
					if ($this->value_len > $this->char_max) {
						$error_count ++;
						$this->is_error = TRUE;
						$this->error = 'Exceed limit maximum ' . $this->char_min . ' characters';
						$this->error_array [$error_count] = $this->error;
					}
				}
			}
			if ($this->type == "EMAIL") {
				if (filter_var ( $str, FILTER_VALIDATE_EMAIL ) === false) {
					$error_count ++;
					$this->is_error = TRUE;
					$this->error = 'Invalid email address';
					$this->error_array [$error_count] = $this->error;
				}
			}
		}
		return $this->is_error;
	}
}
class FormObjSelectInput {
	public function __construct($name, $options, $required) {
	}
}