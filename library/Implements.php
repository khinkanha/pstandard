<?php

interface SendMailInterface
{

    function send_html($sender_email, $sender_name, $receiver_email, $subject, $content);

    static public function SendHtml($sender_email, $sender_name, $receiver_email, $subject, $content);
}
