<?php
class MCache {
	private static $_instance = null;
	protected static $count = 0;
	public $memcache = NULL;
	public $has_module = FALSE;
	private $is_memcached = FALSE;
	private $isMemcacheAvailable = FALSE;
	public function __construct($server_host = NULL, $port = 11211) {
		if (class_exists ( 'Memcache' )) {
			$this->memcache = new Memcache ();
			$this->has_module = TRUE;
		} elseif (class_exists ( 'Memcached' )) {
			$this->memcache = new Memcached ();
			$this->has_module = TRUE;
			$this->is_memcached = TRUE;
		}
		
		if ($this->has_module == TRUE) {
			if ($this->memcache) {
				if ($server_host == NULL) {
					if (! empty ( $_ENV ['memcached_host'] )) {
						$server_host = $_ENV ['memcached_host'];
					} else {
						$server_host = 'localhost';
					}
				}
				$this->memcache->addServer ( $server_host, $port );
			}
			
			// if ($this->memcache->connect($server_host, $port)) {
			// $this->isMemcacheAvailable = TRUE;
			// }
		}
		if (! $this->isMemcacheAvailable) {
			// echo 'Memcached is not enabled';
			// exit ();
		}
		if (! $this->has_module) {
			echo 'Memcache(d) PHP Module is not installed';
			exit ();
		}
	}
	public static function getInstance() {
		if (! isset ( self::$_instance )) {
			self::$_instance = new MCache ();
		}
		return self::$_instance;
	}
	public function get($key) {
		self::$count ++;
		return $this->memcache->get ( $key );
	}
	
	/**
	 * Add and Update record
	 *
	 * @param string $key
	 * @param array|string $value
	 * @param integer $expiry
	 *        	in seconds, maxmum 30 days, -1 Never Expired, 0 = No Cache
	 * @return boolean
	 */
	public function set($key, $value, $expiry = 0) {
		if ($this->is_memcached) {
			return $this->memcache->set ( $key, $value, $expiry );
		} else {
			return $this->memcache->set ( $key, $value, 0, $expiry );
		}
	}
	
	/**
	 * Add record to memcached, return FALSE if record is already exist
	 *
	 * @param string $key
	 * @param array|string $value
	 * @param integer $expiry
	 *        	in seconds, maxmum 30 days
	 * @return boolean
	 */
	public function add($key, $value, $expiry = 0) {
		if ($this->is_memcached) {
			return $this->memcache->add ( $key, $value, $expiry );
		} else {
			return $this->memcache->add ( $key, $value, 0, $expiry );
		}
	}
	
	/**
	 * Flush all existing items at the server, immediately invalidates all existing items.
	 *
	 * @return bool
	 */
	public function flush() {
		return $this->memcache->flush ();
	}
	
	/**
	 * Delete item from the server
	 *
	 * @param string $key
	 * @return bool
	 */
	public function delete($key) {
		return $this->memcache->delete ( $key );
	}
	
	/**
	 * Increment item's value, increments value of an item by the specified value.
	 * If item specified by key was not numeric and cannot be converted to a number, it will change its value to value.
	 *
	 * @param string $key
	 * @param number $value
	 * @return integer
	 * @see decrement()
	 */
	public function increment($key, $value = 1) {
		return $this->memcache->increment ( $key, $value );
	}
	
	/**
	 * Decrement item's value, current value of the item is being converted to numerical and after that value is subtracted.
	 * New item's value will not be less than zero.
	 *
	 * @param string $key
	 * @param number $value
	 * @return integer
	 * @see increment()
	 */
	public function decrement($key, $value = 1) {
		return $this->memcache->decrement ( $key, $value );
	}
	public static function getTotalOfCache() {
		return self::$count;
	}
	public function __destruct() {
		if ($this->isMemcacheAvailable) {
			$this->memcache->close ();
		}
	}
}