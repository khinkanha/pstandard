<?php

class Router
{

    private static $_routes;

    private static $_pattern = array();

    private static $_replacement = array();

    private static $count = 0;

    public function __construct($args = [])
    {
        // Nothing to do here
    }

    /**
     * Add router, this must use within Config file, visit website https://www.phpliveregex.com/ to learn more about PHP Regex
     *
     * @param string $regex
     * @param string $replacement
     * @example Router::add('@^view/([0-9]+)/.+@i','page_view/$1');
     */
    static public function add($regex, $replacement)
    {
        self::$count ++;
        self::$_pattern[self::$count] = $regex;
        self::$_replacement[self::$count] = $replacement;
        
        // Nothing to do yet
    }

    /**
     * Only call within the Core class
     *
     * @param string $url
     * @return string
     */
    static public function apply($url)
    {
        if (self::$count > 0) {
            for ($i = 1; $i <= self::$count; $i ++) {
                $new_str = preg_replace(self::$_pattern[$i], self::$_replacement[$i], $url);
                if ($new_str != $url) {
                    return $new_str;
                }
            }
        } else {
            return $url;
        }
    }
}