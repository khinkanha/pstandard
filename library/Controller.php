<?php

/**
 * 
 * @author Controller
 *
 */
class Controller
{

    public $_args = array();

    /**
     * Variable to pass to View
     *
     * @var stdClass
     */
    public $vars;

    function __construct($args = array())
    {
        $this->_args = $args;
        if (count($args) > 1) {
            array_splice($this->_args, 0, 2);
        }
        $this->vars = new stdClass();
        $this->view = new View();
    }

    function set_args(array $array_args)
    {
        $this->_args = $array_args;
    }

    function index()
    {}

    function __call($c_name, $c_args)
    {}

    function _view($view)
    {
        $this->view->render($view, $this->vars);
    }
}