<?php

class DB
{

    private static $_instance = null;

    protected static $count = 0;

    protected $connection = null;

    protected $transactionLevel = 0;

    protected $rowCount = 0;

    private static $sql_query = null;
    
    /**
     *
     * @var PDO
     */
    public $pdo;

    private $_query, $_error = false, $_results, $_count = 0;

    private function __construct()
    {
    	global $db_host, $db_name, $db_user, $db_pass, $db_port;
    	$default_port = 3306;
    	if (isset($db_port) && is_numeric($db_port)) {
    		$default_port = $db_port;
    	}
        try {
            $timezone = date_default_timezone_get();
            $this->pdo = new PDO('mysql:host=' . $db_host . ';port=' . $default_port . ';dbname=' . $db_name, $db_user, $db_pass);
            $this->pdo->exec("SET NAMES UTF8");
            $this->pdo->exec("SET CHARACTER SET utf8mb4");
            $this->pdo->exec("SET time_zone = '{$timezone}'");
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    /**
     * Instantiate a new database connection if database has not been connected yet, other wise return existing connection
     *
     * @return DB
     */
    public static function getInstance()
    {
        if (! isset(self::$_instance)) {
            self::$_instance = new DB();
        }
        return self::$_instance;
    }

    /**
     * Get result of the database query
     *
     * @param string $query
     *            A database query string
     * @param array $param
     *            An array of parameter for query's placeholder
     * @param integer $cache_time
     *            0 = No Cache, -1 = No expiry
     * @param string $key
     *            memcache custom key
     * @return mixed Query result or FALSE if there is no data
     */
    public function select($query, $param = null, $cache_time = 0, $mkey = NULL)
    {
        global $enable_memcache;
        $sh = $this->pdo->prepare($query);
        $param_str = NULL;
        if (is_array($param)) {
            foreach ($param as $key => $value) {
                if (is_array($value)) {
                    $sh->bindValue(':' . $key, $value[0], $value[1]);
                } else {
                    $sh->bindValue(':' . $key, $value);
                }
            }
            $param_str = implode(",", $param);
        }
        $records = NULL;
        $key = NULL;
        /**
         * Define custom key or automatical key for memcache
         */
        if ($mkey != NULL) {
            $key = $mkey;
        } else {
            $key = md5($sh->queryString . $param_str);
        }
        if (($cache_time != 0) && $enable_memcache) {
            $cache = MCache::getInstance();
            $records = $cache->get($key);
            if ($records || is_array($records)) {
                return $records;
            }
        }
        self::$count ++;
        $sh->execute();
        $result = $sh->fetchAll();
        self::$sql_query .= "<li>" . $sh->queryString . " [" . $param_str . "]</li>\n";
        if (($cache_time != 0) && $enable_memcache) {
            $cache = MCache::getInstance();
            if ($cache_time == - 1) {
                $cache_time = 0;
            }
            $cache->set($key, $result, $cache_time);
        }
        return $result;
    }

    public static function getTotalOfQuery()
    {
        return self::$count;
    }

    public static function getQueryDebug() {
    	return self::$sql_query;
    }
    
    /**
     * Get a first row data of the query
     *
     * @param string $query
     *            A database query string
     * @param array $param
     *            An array of parameter for query's placeholder
     * @return mixed First row data of the query or FALSE if there is no data
     */
    public function first($query, $param = null, $cache_time = 0, $mkey = NULL)
    {
        $result = $this->select($query, $param, $cache_time, $mkey);
        if ($result) {
            return $result[0];
        }

        return false;
    }

    /**
     * Execute database query without returning the result
     *
     * @param string $query
     *            A database query string
     * @param array $param
     *            An array of parameter for query's placeholder
     * @return mixed TRUE if there is no error, otherwise FALSE
     */
    public function query($query, $param = null)
    {
        $sh = $this->pdo->prepare($query);
        self::$count ++;

        if (is_array($param)) {
            foreach ($param as $key => $value) {
                $sh->bindValue(':' . $key, $value);
            }
        }

        $r = $sh->execute();
        $this->rowCount = $sh->rowCount();
        return $r;
    }

    public function getLastInsertId()
    {
        return $this->pdo->lastInsertId();
    }

    /**
     * Wrap group of query into one transaction.
     *
     * @param string $callback
     *            a function that wrap all query in one transaction
     */
    public function transaction($callback)
    {
        $this->beginTransaction();

        try {
            $callback($this);

            $this->commit();
        } catch (Exception $ex) {
            $this->rollback();
        }
    }

    /**
     * Create transaction
     */
    public function beginTransaction()
    {
        if ($this->transactionLevel <= 0) {
            $this->transactionLevel = 0;
            $this->pdo->beginTransaction();
        }

        $this->transactionLevel ++;
    }

    /**
     * Rollback all the changes in current transaction
     *
     * @return boolean
     */
    public function rollback()
    {
        if (-- $this->transactionLevel == 0) {
            return $this->pdo->rollback();
        } else {
            throw new Exception("Invalid depth of transaction.");
        }
    }

    public function rowCount()
    {
        return $this->rowCount;
    }

    /**
     * Commit all the changes.
     *
     * @return string
     */
    public function commit()
    {
        if (-- $this->transactionLevel == 0) {
            $this->pdo->commit();
        }

        return $this->transactionLevel > 0;
    }
}
