<?php

/**
 * Master class for Model representing a table
 * @author mvc
 *
 */
class Master
{

    /**
     * Table name that this class is binded to
     *
     * @var string
     */
    protected $table_name = NULL;

    /**
     * Primary key
     *
     * @var string
     */
    protected $key_name = NULL;

    protected $data = array();

    public $rawResult = NULL;

    private $enable_cache = FALSE;

    /**
     * Cache expired by default 120 seconds
     *
     * @var integer
     */
    private $cache_time = 0;

    private $custom_key = NULL;

    /**
     * Construct the object
     *
     * @param boolean $enable_cache
     *            enable pull_by_id cache
     * @param number $cache_time
     *            default cache time of 300 seconds
     * @param string $key
     *            customer cache key
     */
    public function __construct($enable_cache = FALSE, $cache_time = 300, $key = NULL)
    {
        // Incase of key is not given, identify the primary key
        if (($this->table_name) && (! $this->key_name)) {
            $db = DB::getInstance();
            $row = $db->first("SHOW KEYS FROM `" . $this->table_name . "` WHERE Key_name = 'PRIMARY'");
            $this->key_name = $row['Column_name'];
        }
        if ($enable_cache) {
            $this->enable_cache = $enable_cache;
            $this->cache_time = $cache_time;
            $this->custom_key = $key;
        }
        $this->vars = new stdClass();
    }

    /**
     * Map PDORow into variable
     *
     * @param PDORow $row
     */
    protected function _mapRow($row)
    {
        while (key($row)) {
            $value = current($row);
            if (isset($this->{key($row)})) {
                settype($value, gettype($this->{key($row)}));
                $this->{key($row)} = $value;
            } else {
                $this->{key($row)} = $value;
            }
            next($row);
        }
    }

    /**
     * Pull a record and map into the object
     *
     * @param integer|string $id_number
     */
    public function pull_by_id($id_number)
    {
        if ($this->key_name != NULL) {
            $db = DB::getInstance();
            $stm = $db->first("SELECT * FROM `" . $this->table_name . "` WHERE `" . $this->key_name . "` = :param1", array(
                'param1' => $id_number
            ), $this->cache_time, $this->custom_key);
            if ($stm) {
                $this->_mapRow($stm);
            }
        }
    }

    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }

    public function __get($name)
    {
        if (array_key_exists($name, $this->data)) {
            return $this->data[$name];
        }
        
        $trace = debug_backtrace();
        trigger_error('Undefined property via __get(): ' . $name . ' in ' . $trace[0]['file'] . ' on line ' . $trace[0]['line'], E_USER_NOTICE);
        return null;
    }

    public function __isset($name)
    {
        return isset($this->data[$name]);
    }

    public function __unset($name)
    {
        unset($this->data[$name]);
    }
}