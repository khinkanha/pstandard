<?php

class Table
{

    private $header = "";

    private $footer = "";

    private $col_cnt = 0;

    /**
     *
     * @var String
     * @tutorial Record table header information ->add_header($info)
     * @see Table::add_header
     */
    private $thead = "";

    /**
     *
     * @var Number
     * @tutorial Number of rows in this table
     */
    private $row_cnt = 0;

    /**
     *
     * @var Number
     * @tutorial Number of colume in the current row, this should not more than total colume count
     */
    private $row_col_cnt = 0;

    private $allrow = "";

    public $border = TRUE;

    public $striped = TRUE;

    public $hover = TRUE;

    public $border_less = FALSE;

    /**
     * Use header THEAD
     *
     * @var string
     */
    public $header_darker = TRUE;

    /**
     * Only make header Dark
     *
     * @var string
     */
    public $header_dark = FALSE;

    public $responsive = TRUE;

    public $style_dark = FALSE;

    private $extra_class = NULL;

    /**
     * Construct table
     *
     * @param number $col_cnt
     * @param string $extra_class
     */
    public function __construct($col_cnt = 0, $extra_class = NULL)
    {
        $this->extra_class = $extra_class;
        $this->col_cnt = $col_cnt;
        $this->footer = '</table>' . "\n";
    }

    /**
     *
     * @param String $info
     * @param String $more_class
     * @tutorial Adding the header of Table
     * @see Table::$thead
     */
    public function add_header($info, $more_class = NULL)
    {
        $this->col_cnt = $this->col_cnt + 1;
        if ($more_class != NULL) {
            $this->thead .= "<th class=\"" . $more_class . "\">$info</th>\n";
        } else {
            $this->thead .= "<th>$info</th>\n";
        }
    }

    public function add_row()
    {
        if ($this->row_col_cnt == $this->col_cnt) {
            $this->allrow .= "</tr>";
            $this->row_col_cnt = 0;
        }
        // Clean Up the old row
        if (($this->row_col_cnt > 0) && ($this->row_col_cnt < $this->col_cnt)) {
            for ($i = $this->row_col_cnt; $i < $this->col_cnt; $i ++) {
                $this->allrow .= "<td></td>\n";
            }
            $this->allrow .= "</tr>\n";
            $this->row_col_cnt = 0;
        }
    }

    /**
     *
     * @param string $info
     * @param number $color_code
     *            <ul>
     *            <li>1 - active</li>
     *            <li>2 - success</li>
     *            <li>3 - info</li>
     *            <li>4 - warning</li>
     *            <li>5 - danger</li>
     *            </ul>
     * @param string $more_class
     *            <ul>
     *            <li>text-left</li>
     *            <li>text-center</li>
     *            <li>text-right</li>
     *            <li>text-justify</li>
     *            <li>text-nowrap</li>
     *            </ul>
     */
    public function add_col($info, $color_code = 0, $more_class = NULL)
    {
        $extra_class = $this->get_ccode($color_code);
        if ($this->row_col_cnt == 0) {
            $this->allrow .= "<tr";
            if ($extra_class != NULL) {
                $this->allrow .= " class=\"" . $extra_class . "\"";
            }
            $this->allrow .= ">\n";
        }
        if ($more_class != NULL) {
            $this->allrow .= "<td class=\"" . $more_class . "\">$info</td>\n";
        } else {
            $this->allrow .= "<td>$info</td>\n";
        }
        $this->row_col_cnt = $this->row_col_cnt + 1;
        
        // @todo - Raise ERROR if row_col_cnt is bigger than actual col_cnt
    }

    public function generate()
    {
        // To finalize the previous row
        $this->add_row();
        
        if ($this->responsive) {
            $this->header = '<div class="table-responsive">' . "\n";
        }
        $this->header .= '<table class="table ';
        if ($this->style_dark) {
            $this->header .= "table-dark ";
        }
        if ($this->border_less) {
            $this->header .= "table-borderless ";
        } elseif ($this->border) {
            $this->header .= "table-bordered ";
        }
        if ($this->hover) {
            $this->header .= "table-hover ";
        }
        if ($this->striped) {
            $this->header .= "table-striped ";
        }
        $this->header .= $this->extra_class . '">' . "\n";
        
        $table = $this->header;
        if (strlen($this->thead) > 0) {
            if ($this->header_darker || $this->header_dark) {
                $table .= "<thead";
                if ($this->header_dark) {
                    $table .= ' class="thead-dark"';
                }
                $table .= ">\n";
            }
            $table .= "<tr>\n";
            $table .= $this->thead;
            $table .= "</tr>\n";
            if ($this->header_darker || $this->header_dark) {
                $table .= "</thead>\n";
            }
        }
        $table .= "<tbody>\n";
        $table .= $this->allrow;
        $table .= "</tbody>\n";
        if ($this->responsive) {
            $this->footer .= "</div>\n";
        }
        $table .= $this->footer;
        return $table;
    }

    private function get_ccode($color_code)
    {
        switch ($color_code) {
            case 0:
                return NULL;
                break;
            case 1:
                return 'active bg-primary';
                break;
            case 2:
                return 'success bg-success';
                break;
            case 3:
                return 'info bg-info';
                break;
            case 4:
                return 'warning bg-warning';
                break;
            case 5:
                return 'danger bg-danger';
                break;
        }
    }

    public function set_col_count($amount)
    {
        $this->col_cnt = $amount;
    }
}