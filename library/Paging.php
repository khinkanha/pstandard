<?php

/**
 * Paging Helper Class
 * @author User
 *
 */
class Paging
{

    private $total_record = 0;

    private $record_per_page = 0;

    private $current_page = 0;

    private $link_url;

    /**
     * Total pages
     *
     * @var integer
     */
    public $total_pages = 0;

    /**
     *
     * @param integer $total_record
     * @param integer $record_per_page
     * @param integer $current_page
     */
    public function __construct($total_record, $record_per_page, $current_page)
    {
        $this->total_pages = $total_record;
        $this->record_per_page = $record_per_page;
        $this->current_page = $current_page;
        $this->total_pages = ceil($total_record / $record_per_page);
    }

    /**
     * URL that ended with number of page
     *
     * @param string $link_url
     */
    public function set_link($link_url)
    {
        $this->link_url = $link_url;
    }

    public function generate_link()
    {
        $str = '<ul class="pagination">';
        $start = "";
        $end = "";
        $i = 1;
        $y = 0;
        $end_page = 0;
        if ($this->total_pages > 1) {
            $begin = $this->current_page - 1;
            if ($begin <= 0) {
                $begin = 1;
            }
            
            if ($this->current_page > 1) {
                $start = '<li class="page-item"><a class="page-link" href="' . $this->link_url . $begin . '" aria-label="Previous"><span aria-hidden="true">&laquo;</span> <span class="sr-only">Previous</span></a></li>';
            } else {
                $start = '<li class="page-item disabled"><a class="page-link" href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span> <span class="sr-only">Previous</span></a></li>';
            }
            if ($this->current_page < $this->total_pages) {
                $end = '<li class="page-item"><a class="page-link" href="' . $this->link_url . ($this->current_page + 1) . '" aria-label="Next"><span aria-hidden="true">&raquo;</span><span class="sr-only">Next</span></a></li>';
            } else {
                $end = '<li class="page-item disabled"><a class="page-link" href="#" aria-label="Next"> <span aria-hidden="true">&raquo;</span><span class="sr-only">Next</span></a></li>';
            }
            
            if ($this->current_page > 8) {
                $i = $this->current_page - 3;
                if ($i < 1) {
                    $i = 1;
                }
            }
            $end_page = $this->current_page + 4;
            if ($end_page < 9) {
                $end_page = 9;
            }
            if ($end_page > $this->total_pages) {
                $end_page = $this->total_pages;
            }
            if ($this->total_pages > $end_page) {
                $end = $end . '<li class="page-item"><a class="page-link" href="' . $this->link_url . $this->total_pages . '">Last</a></li>';
            }
            if ($i > 1) {
                $start = '<li class="page-item"><a class="page-link" href="' . $this->link_url . '">First</a></li>' . $start;
            }
            $str .= $start;
        }
        for ($i; $i <= $end_page; $i ++) {
            // if ($y < 5) {
            $str .= ' <li class="page-item';
            if ($i == $this->current_page) {
                $str .= ' active';
            }
            $str .= '"><a class="page-link" href="' . $this->link_url . $i . '">' . $i . '</a></li>' . "\n";
            // }
            $y = $y + 1;
        }
        $str .= $end;
        $str .= '</ul>';
        return $str;
    }
}