<?php

class View
{

    public $my_error = NULL;

    public $my_success = NULL;

    /**
     * Location for overloaded data.
     */
    private $data = array();

    /**
     * Overloading not used on declared properties.
     */
    public $declared = 1;

    /**
     * Overloading only used on this when accessed outside the class.
     */
    private $hidden = 2;

    public static $vars;

    function __construct()
    {}

    /**
     *
     * @param string $name
     * @param stdClass $vars
     */
    public static function render($name, $vars = null)
    {
        if ($vars !== null) {
            View::$vars = $vars;
        }
        // Set all class member to internal variable for Template to use
        if (is_object(View::$vars)) {
            $class_vars = get_object_vars(View::$vars);
            foreach ($class_vars as $namee => $value) {
                ${$namee} = $value;
            }
        }
        $require_path = 'views/' . $name . '.php';
        if (file_exists($require_path)) {
            require $require_path;
        } else {
            echo "View not found";
        }
    }

    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }

    public function __get($name)
    {
        if (array_key_exists($name, $this->data)) {
            return $this->data[$name];
        }
        
        $trace = debug_backtrace();
        trigger_error('Undefined property via __get(): ' . $name . ' in ' . $trace[0]['file'] . ' on line ' . $trace[0]['line'], E_USER_NOTICE);
        return null;
    }

    /**
     * As of PHP 5.1.0
     */
    public function __isset($name)
    {
        return isset($this->data[$name]);
    }

    /**
     * As of PHP 5.1.0
     */
    public function __unset($name)
    {
        unset($this->data[$name]);
    }

    /**
     * Not a magic method, just here for example.
     */
    public function getHidden()
    {
        return $this->hidden;
    }
}