-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 21, 2020 at 10:47 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sample`
--

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE IF NOT EXISTS `log` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `amount` decimal(10,4) NOT NULL DEFAULT '0.0000',
  `from_user_id` int(11) NOT NULL DEFAULT '0',
  `gen` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 to 21',
  `type` int(11) NOT NULL DEFAULT '0' COMMENT '0 = Register Bonus, 1=Repurchase Bonus,2=Withdraw',
  `note` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `balance` decimal(12,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`log_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `balance` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `lvl_1_id` int(11) NOT NULL DEFAULT '0',
  `lvl_2_id` int(11) NOT NULL DEFAULT '0',
  `lvl_3_id` int(11) NOT NULL DEFAULT '0',
  `lvl_4_id` int(11) NOT NULL DEFAULT '0',
  `lvl_5_id` int(11) NOT NULL DEFAULT '0',
  `lvl_6_id` int(11) NOT NULL DEFAULT '0',
  `lvl_7_id` int(11) NOT NULL DEFAULT '0',
  `lvl_8_id` int(11) NOT NULL DEFAULT '0',
  `lvl_9_id` int(11) NOT NULL DEFAULT '0',
  `lvl_10_id` int(11) NOT NULL DEFAULT '0',
  `lvl_11_id` int(11) NOT NULL DEFAULT '0',
  `lvl_12_id` int(11) NOT NULL DEFAULT '0',
  `lvl_13_id` int(11) NOT NULL DEFAULT '0',
  `lvl_14_id` int(11) NOT NULL DEFAULT '0',
  `lvl_15_id` int(11) NOT NULL DEFAULT '0',
  `lvl_16_id` int(11) NOT NULL DEFAULT '0',
  `lvl_17_id` int(11) NOT NULL DEFAULT '0',
  `lvl_18_id` int(11) NOT NULL DEFAULT '0',
  `lvl_19_id` int(11) NOT NULL DEFAULT '0',
  `lvl_20_id` int(11) NOT NULL DEFAULT '0',
  `lvl_21_id` int(11) NOT NULL DEFAULT '0',
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_verified` tinyint(1) NOT NULL DEFAULT '0',
  `phone` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fullname` text COLLATE utf8_unicode_ci,
  `level` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `verify_code` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_no` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` enum('newbie','thinkgoal','setgoal','trustgoal','trustlife') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'newbie',
  `sponsor` int(11) NOT NULL DEFAULT '0',
  `direct_dl_cnt` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `username`, `password`, `balance`, `lvl_1_id`, `lvl_2_id`, `lvl_3_id`, `lvl_4_id`, `lvl_5_id`, `lvl_6_id`, `lvl_7_id`, `lvl_8_id`, `lvl_9_id`, `lvl_10_id`, `lvl_11_id`, `lvl_12_id`, `lvl_13_id`, `lvl_14_id`, `lvl_15_id`, `lvl_16_id`, `lvl_17_id`, `lvl_18_id`, `lvl_19_id`, `lvl_20_id`, `lvl_21_id`, `email`, `email_verified`, `phone`, `fullname`, `level`, `created_at`, `verify_code`, `bank_name`, `account_name`, `account_no`, `position`, `sponsor`, `direct_dl_cnt`) VALUES
(1, 'adminuser', '627c0be97dafa04479984650d41d6e9e', '0.0000', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'khinkanha@gmail.com', 0, '017866539', 'Kanha', -1, '2020-02-21 07:53:00', '', 'ABA Bank', 'Khin Kanha', '000144299', 'newbie', 0, 0),
(2, '928749237', '627c0be97dafa04479984650d41d6e9e', '0.0000', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'khinkanha@yahoo.com', 0, '0', 'kanha khin', 0, '2020-02-21 08:52:35', 'IKqn!jZP', NULL, NULL, NULL, 'newbie', 0, 0),
(3, '323487298', 'afa2f30a26a5401b4ee1121f374b81d4', '0.0000', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'khindara@gmail.com', 0, '0', 'Dara Khin', 0, '2020-02-21 08:57:11', 'qOYEvwRJ', NULL, NULL, NULL, 'newbie', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `withdraw`
--

CREATE TABLE IF NOT EXISTS `withdraw` (
  `withdraw_id` int(11) NOT NULL AUTO_INCREMENT,
  `created_by` int(11) NOT NULL,
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `bank_name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `account_name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `account_number` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `batch_id` int(10) NOT NULL DEFAULT '0',
  UNIQUE KEY `withdraw_id` (`withdraw_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `withdraw_batch`
--

CREATE TABLE IF NOT EXISTS `withdraw_batch` (
  `batch_id` int(10) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `total_amount_paid` decimal(10,2) NOT NULL DEFAULT '0.00',
  `batch_status` enum('created','processing','completed') NOT NULL DEFAULT 'created',
  PRIMARY KEY (`batch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
