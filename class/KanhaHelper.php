<?php

class KanhaHelper
{

    public function __construct()
    {}

    /**
     * Post JSON to a services and receive JSON response back
     *
     * @param string $url
     * @param stdClass $data
     * @return stdClass
     */
    static public function PostJson($url, $data)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type:application/json'
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        return json_decode($result);
    }
}