<?php

class LogModel extends Master
{

    protected $key_name = "log_id";

    protected $table_name = "log";

    public $log_id;

    public $user_id;

    public $amount = 0.0;

    public $from_user_id = 0;

    public $gen = 0;

    public $type;

    public $note;

    public $created_at;

    public $balance = 0.0;

    const TYPE_REGISTER_BONUS = 0;

    const TYPE_REPURCHASE_BONUS = 1;

    const TYPE_WITHDRAW = 2;

    public function __construct()
    {
        parent::__construct();
    }

    static public function add_log($user_id, $amount, $from_user_id, $gen, $type, $note, $balance)
    {
        $db = DB::getInstance();
        $data = array(
            'user_id' => $user_id,
            'amount' => $amount,
            'from_user_id' => $from_user_id,
            'gen' => $gen,
            'type' => $type,
            'note' => $note,
            'balance' => $balance
        );
        $stm = $db->query("INSERT INTO log (user_id,amount,from_user_id,gen,type,note,balance,created_at) VALUES(
                            :user_id,:amount,:from_user_id,:gen,:type,:note,:balance,NOW())", $data);
    }

    static public function _list($user_id, $limit, $offset)
    {
        $db = DB::getInstance();
        return $db->select("SELECT *,(SELECT username FROM user WHERE user.user_id=log.from_user_id) AS username,(SELECT username FROM user WHERE user.user_id=log.user_id) AS for_user FROM log WHERE user_id=:user_id ORDER BY log_id DESC LIMIT $limit OFFSET $offset;", array(
            'user_id' => $user_id
        ));
    }

    static public function get_total($user_id = 0)
    {
        $db = DB::getInstance();
        if ($user_id > 0) {
            $stm = $db->first("SELECT COUNT(log_id) AS total FROM log WHERE user_id=:user_id;", array(
                'user_id' => $user_id
            ));
        } else {
            $stm = $db->first("SELECT COUNT(log_id) AS total FROM log;");
        }
        return $stm->total;
    }
}