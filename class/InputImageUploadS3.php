<?php
require_once 'vendor/autoload.php';
use Aws\S3\S3Client;
class InputImageUploadS3 extends InputImageUpload {
	/**
	 * AWS S3 Access Key
	 * @var string
	 */
	private $access_key;
	/**
	 * AWS S3 Access Secret
	 * @var string
	 */
	private $access_secret;
	/**
	 * AWS S3 Bucket Name
	 * @var string
	 */
	private $bucket_name;
	/**
	 * AWS S3 Bucket URL
	 * @var string
	 */
	private $endpoint;
	/**
	 * AWS S3 Region
	 * @var string
	 */
	private $region;
	/**
	 * 
	 * @param string $inputName
	 * @param string $access_key
	 * @param string $access_secret
	 * @param string $bucket_name
	 * @param string $endpoint AWS S3 End Point URL
	 * @param string $region
	 */
	public function __construct($inputName, $access_key, $access_secret, $bucket_name, $endpoint, $region) {
		$this->access_key = $access_key;
		$this->access_secret = $access_secret;
		$this->bucket_name = $bucket_name;
		$this->endpoint = $endpoint;
		$this->region = $region;
		parent::__construct ( $inputName );
	}
	
	
	public function upload_to($to_path, $file_name = NULL) {
		$uploadPath = NULL;
		if ($to_path [strlen ( $to_path ) - 1] != '/') {
			$to_path .= '/';
		}
		if ($file_name == NULL) {
			$uploadPath = $to_path . basename ( $this->fileName );
		} else {
			$uploadPath = $to_path . basename ( $file_name ) . '.' . $this->fileExtension;
			$this->fileName = $file_name . '.' . $this->fileExtension;
		}
				
		$client = new Aws\S3\S3Client ( [
				'version' => 'latest',
				'region' => $this->region,
				'endpoint' => $this->endpoint,
				'credentials' => [
						'key' => $this->access_key,
						'secret' => $this->access_secret
				]
		] );
		
		$spaces = $client->putObject ( [
				'Bucket' => $this->bucket_name,
				'Key' => $uploadPath,
				'SourceFile' => $this->fileTmpName,
				'StorageClass' => 'REDUCED_REDUNDANCY',
				'ACL' => 'public-read'
		] );
		
		if ($spaces) {
			return TRUE;
		}
		return FALSE;
	}
}

