<?php

/**
 * Handling Logged In Controller
 */
class UserController extends Controller
{

    /**
     * Current Logged In User
     *
     * @var UserModel
     */
    public $user = NULL;

    /**
     * If this user is an admin
     *
     * @var boolean
     */
    public $is_admin = FALSE;

    public function __construct($args = [])
    {
        parent::__construct($args);
        $this->user = new UserModel();
        if (isset($_SESSION['user_id'])) {
            $this->user->pull_by_id(intval($_SESSION['user_id']));
            if ($this->user->level == '-1') {
                $this->is_admin = TRUE;
            }
            $this->vars->is_admin = $this->is_admin;
        }
        if ($this->user->user_id == 0) {
            header('Location: /login/');
            exit();
        }
        $this->vars->user = $this->user;
    }
}