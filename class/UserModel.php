<?php

class UserModel extends Master
{

    protected $table_name = "user";

    protected $key_name = "user_id";

    public $user_id = 0;

    public $username;

    public $password;

    public $balance = 0.0;

    public $email;

    public $email_verified;

    public $phone;

    public $fullname;

    public $level = 0;

    public $created_at;

    public $verify_code;

    public $bank_name;

    public $account_name;

    public $account_no;

    public $position;
    public $secure_code;

    public function __construct()
    {
        parent::__construct();
    }
    public function updateSecurecode($code)
    {
        $db = DB::getInstance();
        $db->query("UPDATE user SET secure_code=:code WHERE user_id=:user_id", array(
            'code' => $code,
            'user_id' => $this->user_id
        ));
    }
    /**
     *
     * @param string $username            
     * @param string $password            
     * @param string $email            
     * @param string $fullname            
     * @param string $phone            
     * @param string $verify_code            
     * @return string|number
     */
    public function register_user($username, $password, $email, $fullname, $phone, $verify_code)
    {
        global $is_debug, $db_str;
        $db = DB::getInstance();
        $data = array(
            'username' => $username,
            'password' => md5($password),
            'email' => $email,
            'fullname' => $fullname,
            'phone' => $phone,
            'verify_code' => $verify_code
        );
        $stm = $db->query("INSERT INTO user(username, password, email, fullname, phone, verify_code )
                    VALUES(:username,:password, :email, :fullname, :phone, :verify_code)", $data);
        if ($stm) {
            return $db->getLastInsertId();
        }
        
        return 0;
    }

    /**
     *
     * @param string $username            
     * @return number else 0 if not have
     */
    public function check_exist_username($username)
    {
        $db = DB::getInstance();
        $stm = $db->first(" SELECT user_id FROM user WHERE username=:username ", array(
            'username' => $username
        ));
        if ($stm != FALSE) {
            return $stm->user_id;
        }
        return 0;
    }

    public function getbyUsername($username)
    {
        $db = DB::getInstance();
        $row = $db->first("SELECT * FROM " . $this->table_name . " WHERE username = :username", array(
            'username' => strtoupper($username)
        ));
        if ($row) {
            $this->_mapRow($row);
        }
        return $this->user_id;
    }

    static public function getUsername($username)
    {
        $db = DB::getInstance();
        $row = $db->first("SELECT * FROM user WHERE username = :username", array(
            'username' => strtoupper($username)
        ));
        if ($row) {
            return $row;
        }
    }

    static public function _list($search = "", $limit, $offset)
    {
        $db = DB::getInstance();
        if ($search) {
            $data = array(
                'username' => ('%' . $search . '%')
            );
            return $db->select("SELECT * FROM user WHERE username LIKE :username OR fullname LIKE :username ORDER BY user_id DESC LIMIT $limit OFFSET $offset;", $data);
        } else {
            return $db->select("SELECT * FROM user  ORDER BY user_id DESC LIMIT $limit OFFSET $offset;");
        }
    }

    static public function get_total_record($crit)
    {
        $db = DB::getInstance();
        if ($crit) {
            $data = array(
                'search_crit' => ('%' . $crit . '%')
            );
            $stm = $db->first("SELECT Count('user_id') AS total FROM user WHERE username like :search_crit OR fullname LIKE :search_crit;", $data);
        } else {
            $stm = $db->first("SELECT Count('user_id') AS total FROM user;");
        }
        if ($stm != "") {
            return $stm->total;
        }
    }

    static public function updatePassword($user_id, $password)
    {
        $db = DB::getInstance();
        $db->query("UPDATE user SET password=:password WHERE user_id=:id;", array(
            'password' => md5($password),
            'id' => $user_id
        ));
    }

    static public function update_info($name, $email, $tel, $user_id)
    {
        $db = DB::getInstance();
        $db->query("UPDATE user SET fullname=:name,email=:email,phone=:tel WHERE user_id=:id;", array(
            'name' => $name,
            'email' => $email,
            'tel' => $tel,
            'id' => $user_id
        ));
    }

    static public function update_bank($bank_name, $account_name, $account_no, $user_id)
    {
        $db = DB::getInstance();
        $db->query("UPDATE user SET bank_name=:name,account_name=:acc_name,account_no=:acc_no WHERE user_id=:id;", array(
            'name' => $bank_name,
            'acc_name' => $account_name,
            'acc_no' => $account_no,
            'id' => $user_id
        ));
    }
    static public function getByEmail($email)
    {
        $db = DB::getInstance();
        $row = $db->first("SELECT * FROM user WHERE email = :email", array(
            'email' => strtoupper($email)
        ));
        if ($row) {
            return $row;
        }
    }
    public static function updateEmailCode($userid, $code)
    {
        $db = DB::getInstance();
        $db->query("UPDATE user SET verify_code=:email_code WHERE user_id=:userid", array(
            "email_code" => $code,
            "userid" => $userid
        ));
    }
}