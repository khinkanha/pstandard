<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require_once ("class/phpmailer/vendor/autoload.php");
function welcome_mail($mailto, $subject = "", $obj, $password,$verify_code)
{
    global $site_url,$title;
    
    
    $body = '<p>Hi there!</p>
			Login Name: ' . $obj->username . '<br/>
			Password: ' . $password . '<br/>
			Link: '.$site_url.' <br/>
			Please click this link '.$site_url.'/verify/'.$verify_code.' to verify your account!<br/>
			This message was generated automatically.<br/>
			If you need help or have questions, email us anytime.
		    <p>
			<p>Sincerely,<br/>Website Support Team</p>';
    
    // Send Welcome to user
    send_email($mailto, $obj->fullname, $subject, $body);
}

function password_reset_mail($mailto, $mailname, $r_username = "", $email_code)
{
    global $site_url,$title;
    
    $subject = "Reset Password from ".ucwords($title);
    $link_code = $site_url . '/reset_password/' . $r_username . '/' . $email_code;
    $body = '<p>Dear ' . $mailname . '<br/>
			Your password reset code is: '. $email_code .'<br />
			Please visit ' . $link_code. ' to complete the password reset<br/>
			For security reasons it is highly recommended to make a strong password.</p>';
    
    // Send Reset Password to user
    
    send_email($mailto, $mailname, $subject, $body);
}

function notify_success_change($mailto, $mailname)
{
    $ip = $_SERVER['REMOTE_ADDR'];
    $subject = "Password change notification";
    $body = '<p>Dear ' . $mailname . ',</p>
			<p>Your password has been successfully changed.</p>';
    send_email($mailto, $mailname, $subject, $body);
}

function send_email($mailto, $mailname, $subject, $content)
{
  global $is_debug;
  global $site_url,$title,$email,$email_password;
  if($is_debug==TRUE)
  {
      return true;
      exit();
  }
        // Create a new PHPMailer instance
    $mail = new PHPMailer();
    // Set who the message is to be sent from
    $mail->setFrom($email, ucwords($title));
    // Set an alternative reply-to address
    $mail->addReplyTo($email, ucwords($title));
    // Set who the message is to be sent to
    $mail->addAddress($mailto, $mailname);
    // Set the subject line
    $mail->Subject = $subject;
    $mail->msgHTML($content);
    // Set the hostname of the mail server
    $mail->Host = "us2.smtp.mailhostbox.com";
    // Set the SMTP port number - likely to be 25, 465 or 587
    $mail->Port = 25;
    
    // Whether to use SMTP authentication
    $mail->IsSMTP();
    $mail->SMTPAuth = true;
    // Username to use for SMTP authentication
    $mail->Username = $email;
    // Password to use for SMTP authentication
    $mail->Password = $email_password;
    
    // send the message, check for errors
    if (! $mail->send()) {
        // echo $mail->ErrorInfo;
        return 0;
    } else {
        return 1;
    }
}
?>