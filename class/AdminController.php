<?php

/**
 * Handling Logged In Controller
 */
class AdminController extends UserController
{

    /**
     * Current Logged In User
     *
     * @var UserModel
     */
    public function __construct($args = [])
    {
        parent::__construct($args);
        
        if ($this->user->user_id == 0) {
            header('Location: /login/');
            exit();
        }
        if (! $this->is_admin) {
            header('Location: /dashboard/');
            exit();
        }
    }
}