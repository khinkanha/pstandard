<?php

class WithdrawModel extends Master
{

    protected $table_name = "withdraw";

    protected $key_name = "withdraw_id";

    public $withdraw_id;

    public $created_by;

    public $amount;

    public $created_at;

    public $bank_name;

    public $account_name;

    public $account_number;

    public $batch_id;

    public function __construct()
    {
        parent::__construct();
    }

    public function add()
    {
        $db = DB::getInstance();
        $data = array(
            'created_by' => $this->created_by,
            'amount' => $this->amount,
            'bank_name' => $this->bank_name,
            'account_name' => $this->account_name,
            'account_number' => $this->account_number,
            'batch_id' => $this->batch_id
        );
        $db->query("INSERT INTO withdraw(created_by,amount,created_at,bank_name,account_name,account_number,batch_id) VALUES(:created_by, :amount, NOW(), :bank_name, :account_name, :account_number, :batch_id)", $data);
    }

    static public function update_status()
    {
        $db = DB::getInstance();
        $db->query("UPDATE withdraw set withdraw_status='completed' WHERE withdraw_status='pending'");
    }

    // get new query after one week
    static public function get_my_withdraw_list($user_id, $limit, $offset)
    {
        $db = DB::getInstance();
        return $db->select("SELECT * ,(SELECT wb.batch_status FROM withdraw_batch wb WHERE wb.batch_id=batch_id) AS wit_status FROM withdraw WHERE created_by = :user_id ORDER BY withdraw_id DESC LIMIT $limit OFFSET $offset;", array(
            'user_id' => $user_id
        ), 604800, "mywithdraw_list");
    }

    static public function get_total_record($crit)
    {
        $db = DB::getInstance();
        if ($crit) {
            $stm = $db->first("SELECT COUNT(withdraw_id) as trecord FROM withdraw WHERE created_by=:created_by; ", array(
                'created_by' => $crit
            ));
        } else {
            $stm = $db->first("SELECT COUNT(withdraw_id) as trecord FROM withdraw");
        }
        
        return $stm->trecord;
    }

    static public function get_withdraw_list($crit, $limit, $offset)
    {
        $db = DB::getInstance();
        if ($crit) {
            return $db->select("SELECT * FROM withdraw WHERE created_by=:created_by ORDER BY withdraw_id DESC LIMIT $limit OFFSET $offset;", array(
                'created_by' => $crit
            ));
        } else {
            return $db->select("SELECT * FROM withdraw  ORDER BY withdraw_id DESC LIMIT $limit OFFSET $offset;");
        }
    }

    static public function get_list_by_batchid($batch_id)
    {
        $db = DB::getInstance();
        return $db->select("SELECT * FROM withdraw WHERE batch_id=:batch_id ORDER BY withdraw_id DESC;", array(
            'batch_id' => $batch_id
        ), 3600, "batch_withdraw" . $batch_id);
    }
}