<?php

class WithdrawBatchModel extends Master
{

    protected $key_name = "batch_id";

    protected $table_name = "withdraw_batch";

    public $batch_id;

    public $created_at;

    public $total_amount_paid;

    public $batch_status;

    const STATUS_CREATED = "created";

    const STATUS_PROCESSING = "processing";

    const STATUS_COMPLETED = "completed";

    static public function _list($status = "", $limit, $offset)
    {
        $db = DB::getInstance();
        $crit = "";
        if ($status != "") {
            
            return $db->select("SELECT * FROM withdraw_batch WHERE batch_status=:status ORDER BY batch_id DESC LIMIT $limit OFFSET $offset; ", array(
                'status' => $status
            ));
        }
        return $db->select("SELECT * FROM withdraw_batch ORDER BY batch_id DESC LIMIT $limit OFFSET $offset; ");
    }

    static public function get_total_record($status)
    {
        $crit = "";
        $db = DB::getInstance();
        if ($status != "") {
            $crit = " WHERE batch_status=" . $status;
            $stm = $db->first("SELECT COUNT(batch_id) AS total FROM withdraw_batch WHERE batch_status=:status;", array(
                'status' => $status
            ));
        } else {
            $stm = $db->first("SELECT COUNT(batch_id) AS total FROM withdraw_batch;");
        }
        
        return $stm->total;
    }

    public function update_status($status)
    {
        $db = DB::getInstance();
        $db->query("UPDATE withdraw_batch SET batch_status=:batch_status WHERE batch_id=:batch_id", array(
            'batch_status' => $status,
            'batch_id' => $this->batch_id
        ));
    }
}