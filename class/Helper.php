<?php
class Helper {
	/**
	 * Return time diff in format of ddhh or hhmm or mmss
	 * @param string $time_str
	 */
	static public function time_after($time_str) {
		$dt1 = new DateTime($time_str);
		$dt2 = new DateTime();
		$interval = $dt1->diff($dt2);
		$str = NULL;
		if ($interval->d > 0) {
			$str = sprintf("%0d days %02d hrs", $interval->d, $interval->h);			
		} elseif ($interval->h > 0) {
			$str = sprintf("%02d hrs %02d mins", $interval->h, $interval->i);
		} else {		
			$str = sprintf("%02d mins %02d secs", $interval->i, $interval->s);
		}
		return $str;
	}
}