<?php
session_start ();
$start = microtime ( true );

$site_path = realpath ( dirname ( __FILE__ ) );
define ( '__SITE_PATH', $site_path );
define ( 'PROJECT_ROOT', realpath ( '.' ) );
$this_host = $_SERVER ['SERVER_NAME'];

$application_path = __DIR__;
$global_err = NULL;
require_once ('library/functions.php');
require_once ("library/Router.php");
if (file_exists ( "config.php" )) {
	require_once ("config.php");
} else {
	$global_err .= "<li>Configuration file is missing</li>";
}
require_once ("library/MCache.php");
require_once ("library/DB.php");
require_once ("library/Implements.php");
spl_autoload_register ( function ($class) {
	// include object from library first
	$a_lib = 'library/' . $class . '.php';
	if (file_exists ( $a_lib )) {
		require_once $a_lib;
	} else {
		$a_class = 'class/' . $class . '.php';
		if (file_exists ( $a_class )) {
			require_once $a_class;
		}
	}
} );
if (file_exists ( 'vendor/autoload.php' )) {
	require_once ('vendor/autoload.php');
}
if ($global_err != NULL) {

	echo "<h3>ERROR Found during initialize project</h3>";
	echo "<ul>";
	echo $global_err;
	echo "</ul>";

	exit ();
}

$mysucess = '';
$myerror = '';